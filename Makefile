
#ccflags-y += -DCONFIG_IEEE80211_NOWEP=y
#ccflags-y += -std=gnu89
#ccflags-y += -O2
#CC            = gcc

ccflags-y := -DSW_ANTE
ccflags-y += -DTX_TRACK
ccflags-y += -DHIGH_POWER
ccflags-y += -DSW_DIG
ccflags-y += -DRATE_ADAPT

#enable it for legacy power save, disable it for leisure power save
ccflags-y += -DENABLE_LPS


#ccflags-y := -mhard-float -DCONFIG_FORCE_HARD_FLOAT=y

r8187se-y :=			\
	dev.o			\
	mac80211.o		\
	irq.o			\
	rtl818x.o		\
	rtl8187se/util.o	\
	rtl8187se/init.o	\
	rtl8187se/rx.o		\
	rtl8187se/tx.o		\
	rtl8187se/irq.o		\
	rtl8187se/rf/rtl8225.o

obj-$(CONFIG_R8187SE)	+= r8187se.o

