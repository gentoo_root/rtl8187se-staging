#include "dev.h"
#include "mac80211.h"
#include "irq.h"
#include "rtl8187se/rtl8187se.h"

static DEFINE_PCI_DEVICE_TABLE(rtl_pci_table) = {
	{ PCI_DEVICE(PCI_VENDOR_ID_REALTEK, 0x8199) },
	{}
};

MODULE_AUTHOR("Andrea Merello <andreamrl@tiscali.it>");
MODULE_AUTHOR("Maxim Mikityanskiy <maxtram95@gmail.com>");
MODULE_DESCRIPTION("RTL8187SE PCI wireless driver");
MODULE_LICENSE("GPL");
MODULE_DEVICE_TABLE(pci, rtl_pci_table);

static int __devinit rtl_probe(struct pci_dev *pdev, const struct pci_device_id *id);
static void __devexit rtl_remove(struct pci_dev *pdev);
static int rtl_suspend(struct pci_dev *pdev, pm_message_t state);
static int rtl_resume(struct pci_dev *pdev);

static struct pci_driver rtl_pci_driver = {
	.name		= KBUILD_MODNAME,
	.id_table	= rtl_pci_table,
	.probe		= rtl_probe,
	.remove		= __devexit_p(rtl_remove),
	.suspend	= rtl_suspend,
	.resume		= rtl_resume,
};

static const struct ieee80211_rate rtl818x_rates[] = {
	{ .bitrate = 10, .hw_value = 0, },
	{ .bitrate = 20, .hw_value = 1, },
	{ .bitrate = 55, .hw_value = 2, },
	{ .bitrate = 110, .hw_value = 3, },
	{ .bitrate = 60, .hw_value = 4, },
	{ .bitrate = 90, .hw_value = 5, },
	{ .bitrate = 120, .hw_value = 6, },
	{ .bitrate = 180, .hw_value = 7, },
	{ .bitrate = 240, .hw_value = 8, },
	{ .bitrate = 360, .hw_value = 9, },
	{ .bitrate = 480, .hw_value = 10, },
	{ .bitrate = 540, .hw_value = 11, },
};

static const struct ieee80211_channel rtl818x_channels[] = {
	{ .center_freq = 2412 },
	{ .center_freq = 2417 },
	{ .center_freq = 2422 },
	{ .center_freq = 2427 },
	{ .center_freq = 2432 },
	{ .center_freq = 2437 },
	{ .center_freq = 2442 },
	{ .center_freq = 2447 },
	{ .center_freq = 2452 },
	{ .center_freq = 2457 },
	{ .center_freq = 2462 },
	{ .center_freq = 2467 },
	{ .center_freq = 2472 },
	{ .center_freq = 2484 },
};

static int __devinit rtl_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	struct ieee80211_hw *dev;
	struct rtl_priv *priv;
	u8 reg;
	int err = 0;
	
	if ((err = pci_enable_device(pdev))) {
		PRINT_ERR("Failed to enable PCI device %s", pci_name(pdev));
		goto rtl_probe_exit;
	}
	if ((err = pci_request_regions(pdev, KBUILD_MODNAME))) {
		PRINT_ERR("Failed to obtain PCI resources of %s", pci_name(pdev));
		goto rtl_probe_disable;
	}
	pci_set_master(pdev);
	if ((err = pci_set_dma_mask(pdev, DMA_BIT_MASK(32))) ||
	    (err = pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(32)))) {
		PRINT_ERR("No suitable DMA available for %s", pci_name(pdev));
		goto rtl_probe_release;
	}

	if (!(dev = ieee80211_alloc_hw(sizeof(*priv), &rtl_ieee80211_ops))) {
		err = -ENOMEM;
		PRINT_ERR("ieee80211 alloc failed for %s", pci_name(pdev));
		goto rtl_probe_release;
	}
	priv = dev->priv;
	pci_set_drvdata(pdev, dev);
	SET_IEEE80211_DEV(dev, &pdev->dev);
	priv->pdev = pdev;
	priv->dev = dev;

	if (!(priv->map = pci_iomap(pdev, 1, pci_resource_len(pdev, 1))))
		if (!(priv->map = pci_iomap(pdev, 0, pci_resource_len(pdev, 0)))) {
			PRINT_ERR("Cannot map device %s memory\n", pci_name(pdev));
			goto rtl_probe_free;
		}

	pci_read_config_byte(pdev, 0x05, &reg);
	pci_write_config_byte(pdev, 0x05, reg & ~0x04);

	priv->ops = &rtl8187se_ops;
	priv->irq = 0;
	priv->vif = NULL;
	priv->irq_enabled = false;
	priv->ack_tx_to_ieee = false;
	priv->retry_rts = 7;
	priv->retry_data = 7;
	priv->rdu = false;
////	INIT_WORK(&priv->restart_wq, (void *)rtl_restart_wq);
	spin_lock_init(&priv->irq_th_lock);
	spin_lock_init(&priv->tx_lock);

	BUILD_BUG_ON(sizeof(priv->channels) != sizeof(rtl818x_channels));
	BUILD_BUG_ON(sizeof(priv->rates) != sizeof(rtl818x_rates));

	memcpy(priv->channels, rtl818x_channels, sizeof(rtl818x_channels));
	memcpy(priv->rates, rtl818x_rates, sizeof(rtl818x_rates));

	priv->band.band = IEEE80211_BAND_2GHZ;
	priv->band.channels = priv->channels;
	priv->band.n_channels = ARRAY_SIZE(rtl818x_channels);
	priv->band.bitrates = priv->rates;
	priv->band.n_bitrates = ARRAY_SIZE(rtl818x_rates);
	dev->wiphy->bands[IEEE80211_BAND_2GHZ] = &priv->band;

	dev->flags = IEEE80211_HW_HOST_BROADCAST_PS_BUFFERING |
		     IEEE80211_HW_RX_INCLUDES_FCS |
		     IEEE80211_HW_SIGNAL_DBM;
	dev->vif_data_size = sizeof(struct rtl_vif);
	dev->wiphy->interface_modes = BIT(NL80211_IFTYPE_STATION) |
					BIT(NL80211_IFTYPE_ADHOC);
	dev->queues = 1;

	if (priv->ops->init(priv)) {
		err = -EIO;
		PRINT_ERR("Initialization of %s failed", pci_name(pdev));
		goto rtl_probe_unmap;
	}
	
	if (request_irq(pdev->irq, rtl_interrupt, IRQF_SHARED, KBUILD_MODNAME, dev)) {
		err = -EBUSY;
		PRINT_ERR("Error allocating IRQ %d", pdev->irq);
		goto rtl_probe_unmap;
	} else {
		priv->irq = pdev->irq;
		PRINT_INFO("IRQ %d", pdev->irq);
	}

	if ((err = ieee80211_register_hw(dev))) {
		PRINT_ERR("Cannot register device");
		goto rtl_probe_unmap;
	}

	return 0;

rtl_probe_unmap:
	pci_iounmap(pdev, priv->map);

rtl_probe_free:
	ieee80211_free_hw(dev);
	pci_set_drvdata(pdev, NULL);

rtl_probe_release:
	pci_release_regions(pdev);

rtl_probe_disable:
	pci_disable_device(pdev);

rtl_probe_exit:
	return err;
}

static void __devexit rtl_remove(struct pci_dev *pdev)
{
	struct ieee80211_hw *dev = pci_get_drvdata(pdev);
	struct rtl_priv *priv;

	if (dev) {
		ieee80211_unregister_hw(dev);
		priv = dev->priv;

////		rtl_down(priv);

		priv->ops->clean(priv);

		if (priv->irq) {
			free_irq(pdev->irq, dev);
			priv->irq = 0;
		}

		pci_iounmap(pdev, priv->map);
		ieee80211_free_hw(dev);
		pci_release_regions(pdev);
	}

	pci_set_drvdata(pdev, NULL);
	pci_disable_device(pdev);
}

static int rtl_suspend(struct pci_dev *pdev, pm_message_t state)
{
	pci_save_state(pdev);
	pci_set_power_state(pdev, pci_choose_state(pdev, state));
	return 0;
}

static int rtl_resume(struct pci_dev *pdev)
{
	u32 reg;

	pci_set_power_state(pdev, PCI_D0);
	pci_restore_state(pdev);

	/*
	 * Suspend/Resume resets the PCI configuration space, so we have to
	 * re-disable the RETRY_TIMEOUT register (0x41) to keep PCI Tx retries
	 * from interfering with C3 CPU state. pci_restore_state won't help
	 * here since it only restores the first 64 bytes pci config header.
	 */
	pci_read_config_dword(pdev, 0x40, &reg);
	if (reg & 0x0000ff00)
		pci_write_config_dword(pdev, 0x40, reg & 0xffff00ff);

	return 0;
}

module_pci_driver(rtl_pci_driver);
