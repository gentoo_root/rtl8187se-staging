#pragma once

#include <linux/module.h>
#include <linux/pci.h>
#include <linux/skbuff.h>
#include <linux/semaphore.h>
#include <linux/interrupt.h>
#include <net/mac80211.h>

#define PRINT_ERR(fmt, args...) printk(KERN_ERR KBUILD_MODNAME ": " fmt "\n", ## args)
#define PRINT_WARN(fmt, args...) printk(KERN_WARNING KBUILD_MODNAME ": " fmt "\n", ## args)
#define PRINT_INFO(fmt, args...) printk(KERN_INFO KBUILD_MODNAME ": " fmt "\n", ## args)

#define RTL_DEFAULT_FRAG_THRESHOLD	2342U

struct rtl_priv;

struct rtl_ops {
	void *priv;
	int (*init)(struct rtl_priv *priv);
	void (*clean)(struct rtl_priv *priv);
	bool (*start)(struct rtl_priv *priv);
	void (*stop)(struct rtl_priv *priv);
	void (*rx_enable)(struct rtl_priv *priv);
	void (*interrupt)(struct rtl_priv *priv);
	bool (*set_channel)(struct rtl_priv *priv, short channel);
	int (*tx)(struct rtl_priv *priv, struct sk_buff *skb, int priority, bool morefrag, int rate);
	bool (*is_tx_queue_empty)(struct rtl_priv *priv);
	void (*bss_info_changed)(struct rtl_priv *priv);
	void (*start_beaconing)(struct rtl_priv *priv);
	void (*stop_beaconing)(struct rtl_priv *priv);
	void (*set_mac_address)(struct rtl_priv *priv, u8 *addr);
	u64 (*get_tsf)(struct rtl_priv *priv);
};

struct rtl_priv {
	/* PCI stuff */
	struct pci_dev *pdev;
	void __iomem *map;
	int irq;

	/* IEEE80211 stuff */
	struct ieee80211_hw *dev;
	struct ieee80211_vif *vif;
	struct ieee80211_channel channels[14];
	struct ieee80211_rate rates[12];
	struct ieee80211_supported_band band;
	spinlock_t irq_th_lock;
	spinlock_t tx_lock;
	struct work_struct restart_wq;
	bool ack_tx_to_ieee;

	/* Device specific */
	struct rtl_ops *ops;

	/* Device state */
	bool irq_enabled;
	u8 chtxpwr[15];
	u8 chtxpwr_ofdm[15];
	bool crcmon;
	bool promisc;
	u8 retry_data;
	u8 retry_rts;
	u16 rts;
	bool rdu;
};

struct rtl_vif {
	struct ieee80211_hw *dev;
	bool enable_beacon;
};
