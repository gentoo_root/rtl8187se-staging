#include "irq.h"

irqreturn_t rtl_interrupt(int irq, void *dev_id)
{
	struct ieee80211_hw *dev = dev_id;
	struct rtl_priv *priv = dev->priv;
	unsigned long flags;

	if (!priv->irq_enabled)
		return IRQ_HANDLED;

	spin_lock_irqsave(&priv->irq_th_lock, flags);
	priv->ops->interrupt(priv);
	spin_unlock_irqrestore(&priv->irq_th_lock, flags);

	return IRQ_HANDLED;
}
