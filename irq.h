#pragma once

#include "dev.h"

irqreturn_t rtl_interrupt(int irq, void *dev_id);
