#include "mac80211.h"
#include <linux/etherdevice.h>

static void rtl_tx(struct ieee80211_hw *dev, struct sk_buff *skb)
{
	struct rtl_priv *priv = dev->priv;
	struct ieee80211_hdr *hdr = (struct ieee80211_hdr *)skb->data;
	struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
	int priority = skb_get_queue_mapping(skb);
	bool morefrags = ieee80211_has_morefrags(hdr->frame_control);
	u16 rate = ieee80211_get_tx_rate(dev, info)->bitrate;

	priv->ops->tx(priv, skb, priority+1, morefrags, rate);
}

static int rtl_start(struct ieee80211_hw *dev)
{
	struct rtl_priv *priv = dev->priv;
	PRINT_INFO("rtl_start()");
	return priv->ops->start(priv) ? 0 : -EIO;
}

static void rtl_stop(struct ieee80211_hw *dev)
{
	struct rtl_priv *priv = dev->priv;
	PRINT_INFO("rtl_stop()");
	priv->ops->stop(priv);
}

static int rtl_add_interface(struct ieee80211_hw *dev, struct ieee80211_vif *vif)
{
	struct rtl_priv *priv = dev->priv;
	struct rtl_vif *vif_priv;

	PRINT_INFO("rtl_add_interface()");

	if (priv->vif)
		return -EBUSY;

	switch (vif->type) {
	case NL80211_IFTYPE_STATION:
	case NL80211_IFTYPE_ADHOC:
		break;
	default:
		return -EOPNOTSUPP;
	}

	priv->vif = vif;

	vif_priv = (struct rtl_vif *)&vif->drv_priv;
	vif_priv->dev = dev;
	vif_priv->enable_beacon = false;

	priv->ops->set_mac_address(priv, vif->addr);

	return 0;
}

static void rtl_remove_interface(struct ieee80211_hw *dev, struct ieee80211_vif *vif)
{
	struct rtl_priv *priv = dev->priv;
	PRINT_INFO("rtl_remove_interface()");
	priv->vif = NULL;
}

static int rtl_config(struct ieee80211_hw *dev, u32 changed)
{
	struct rtl_priv *priv = dev->priv;
	if (priv->ops->set_channel(priv, ieee80211_frequency_to_channel(dev->conf.channel->center_freq)))
		return 0;
	else
		return -EINVAL;
}

static void rtl_bss_info_changed(struct ieee80211_hw *dev, struct ieee80211_vif *vif, struct ieee80211_bss_conf *info, u32 changed)
{
	// FIXME: implement it
}

static u64 rtl_prepare_multicast(struct ieee80211_hw *dev, struct netdev_hw_addr_list *mc_list)
{
	return netdev_hw_addr_list_count(mc_list);
}

static void rtl_configure_filter(struct ieee80211_hw *dev, unsigned changed_flags, unsigned *total_flags, u64 multicast)
{
	// FIXME: implement it
	*total_flags = 0;
}

static u64 rtl_get_tsf(struct ieee80211_hw *dev, struct ieee80211_vif *vif)
{
	struct rtl_priv *priv = dev->priv;
	return priv->ops->get_tsf(priv);
}

const struct ieee80211_ops rtl_ieee80211_ops = {
	.tx			= rtl_tx,
	.start			= rtl_start,
	.stop			= rtl_stop,
	.add_interface		= rtl_add_interface,
	.remove_interface	= rtl_remove_interface,
	.config			= rtl_config,
	.bss_info_changed	= rtl_bss_info_changed,
	.prepare_multicast	= rtl_prepare_multicast,
	.configure_filter	= rtl_configure_filter,
	.get_tsf		= rtl_get_tsf,
};
