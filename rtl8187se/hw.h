#pragma once

#include "../dev.h"
#include "rx.h"
#include "tx.h"

struct rtl8187se_priv {
	const struct rtl8187se_rf_ops *rf;
	struct rtl8187se_rx_ring rx_ring;
	struct rtl8187se_tx_ring tx_ring[7];
	u32 rx_conf;
	u32 tx_conf;
};

enum rtl8187se_regs {
	/* MAC address */
	REG_MAC				= 0x00,
	/* Management frames TX ring address */
	REG_TX_MG_RING_ADDR		= 0x0c,
	/* Background frames TX ring address */
	REG_TX_BK_RING_ADDR		= 0x10,
	/* Best effort frames TX ring address */
	REG_TX_BE_RING_ADDR		= 0x14,
	/* Timing Synchronization Function Timer Register */
	REG_TSFT			= 0x18,
	/* Video frames TX ring address */
	REG_TX_VI_RING_ADDR		= 0x20,
	/* Voice frames TX ring address */
	REG_TX_VO_RING_ADDR		= 0x24,
	/* High priority frames TX ring address */
	REG_TX_HI_RING_ADDR		= 0x28,
	/* Basic Service Set Identification */
	REG_BSSID			= 0x2e,
	/* Basic Rate Set Register */
	REG_BRSR			= 0x34,
	/* Command register */
	REG_CMD				= 0x37,
	/* Interrupt status register */
	REG_ISR				= 0x3c,
	/* Transmit config */
	REG_TX_CONF			= 0x40,
	/* Receive config */
	REG_RX_CONF			= 0x44,
	/* Timer interrupt */
	REG_TIMER_INT			= 0x48,
	/* Beacon frames TX ring address */
	REG_TX_BC_RING_ADDR		= 0x4c,
	/* Analog parameters */
	REG_ANAPARAM			= 0x54,
	/* Media status register */
	REG_MSR				= 0x58,
	/* Configuration register 3 */
	REG_CONFIG3			= 0x59,
	/* Analog parameters 2 */
	REG_ANAPARAM2			= 0x60,
	/* Page select register */
	REG_PGSELECT			= 0x5e,
	/* Interrupt mask register */
	REG_IMR				= 0x6c,
	/* Beacon interval */
	REG_BEACON_INTERVAL		= 0x70,
	/* Announcement traffic indication message window */
	REG_ATIM_WND			= 0x72,
	/* TX automatic gain control register */
	REG_TX_AGC_CTL			= 0x9c,
	/* TX CCK gain */
	REG_TX_GAIN_CCK			= 0x9d,
	/* TX OFDM gain */
	REG_TX_GAIN_OFDM		= 0x9e,
	/* TX antenna select */
	REG_TX_ANTENNA			= 0x9f,
	/* WPA config */
	REG_WPA_CONF			= 0xb0,
	/* Contention window config */
	REG_CW_CONF			= 0xbc,
	/* Fallback rate */
	REG_RATE_FALLBACK		= 0xbe,
	/* Configuration register 5 */
	REG_CONFIG5			= 0xd8,
	/* Transmit priority polling register */
	REG_TX_DMA_POLLING		= 0xd9,
	/* PHY Parameter Register */
	REG_PHY_PARAM			= 0xda,
	/* Interrupt Migration */
	REG_INT_MIG			= 0xe2,
	/* RX ring address */
	REG_RX_RING_ADDR		= 0xe4,
	/* TID to AC mapping register */
	REG_TID_AC_MAP			= 0xe8,
	/* Analog parameters 3 */
	REG_ANAPARAM3			= 0xee,
	/* Function event mask register */
	REG_FEMR			= 0x1d4,
	/* Auto rate fallback register */
	REG_ARFR			= 0x1e0,
	/* RFSW control */
	REG_RFSW_CTL			= 0x272,
};

enum rtl8187se_bits_pgselect {
	BIT_PGSELECT_PAGE		= (1 << 0),
};

enum rtl8187se_bits_cmd {
	BIT_CMD_TX_ENABLE		= (1 << 2),
	BIT_CMD_RX_ENABLE		= (1 << 3),
	BIT_CMD_RESET			= (1 << 4),
};

enum rtl8187se_bits_msr {
	BIT_MSR_NO_LINK			= (0 << 2),
	BIT_MSR_ADHOC			= (1 << 2),
	BIT_MSR_INFRA			= (2 << 2),
	BIT_MSR_MASTER			= (3 << 2),
	BIT_MSR_ENEDCA			= (1 << 4),
};

enum rtl8187se_bits_imr {
	/* TX Manage OK interrupt */
	BIT_IMR_TMG_OK			= (1 << 30),	
	/* 802.11h measurement interrupt */
	BIT_IMR_DOT11H			= (1 << 25),
	/* Beacon DMA interrupt */
	BIT_IMR_BEACON_DMA		= (1 << 24),
	/* Wake up interrupt */
	BIT_IMR_WAKE			= (1 << 23),
	/* TX FIFO overflow interrupt */
	BIT_IMR_TX_OVERFLOW		= (1 << 22),
	/* Timeout 1 interrupt */
	BIT_IMR_TIMEOUT1		= (1 << 21),
	/* Beacon timeout interrupt */
	BIT_IMR_BEACON_TIMEOUT		= (1 << 20),
	/* ATIM timeout interrupt */
	BIT_IMR_ATIM_TIMEOUT		= (1 << 19),
	/* TX beacon descriptor error interrupt */
	BIT_IMR_TB_ERR			= (1 << 18),
	/* TX beacon descriptor OK interrupt */
	BIT_IMR_TB_OK			= (1 << 17),
	/* TX high priority descriptor error interrupt */
	BIT_IMR_THP_ERR			= (1 << 16),
	/* TX high priority descriptor OK interrupt */
	BIT_IMR_THP_OK			= (1 << 15),
	/* TX VO descriptor error interrupt */
	BIT_IMR_TVO_ERR			= (1 << 14),
	/* TX VO descriptor OK interrupt */
	BIT_IMR_TVO_OK			= (1 << 13),
	/* RX FIFO overflow interrupt */
	BIT_IMR_RX_OVERFLOW		= (1 << 12),
	/* RX descriptor unavailable interrupt */
	BIT_IMR_RX_UNAVAIL		= (1 << 11),
	/* TX VI descriptor error interrupt */
	BIT_IMR_TVI_ERR			= (1 << 10),
	/* TX VI descriptor OK interrupt */
	BIT_IMR_TVI_OK			= (1 << 9),
	/* RX error interrupt */
	BIT_IMR_RX_ERR			= (1 << 8),
	/* RX OK interrupt */
	BIT_IMR_RX_OK			= (1 << 7),
	/* TX BE descriptor error interrupt */
	BIT_IMR_TBE_ERR			= (1 << 6),
	/* TX BE descriptor OK interrupt */
	BIT_IMR_TBE_OK			= (1 << 5),
	/* TX BK descriptor error interrupt */
	BIT_IMR_TBK_ERR			= (1 << 4),
	/* TX BK descriptor OK interrupt */
	BIT_IMR_TBK_OK			= (1 << 3),
	/* RX QoS OK interrupt */
	BIT_IMR_RQOS_OK			= (1 << 2),
	/* Timeout 2 interrupt */
	BIT_IMR_TIMEOUT2		= (1 << 1),
	/* Timeout 3 interrupt */
	BIT_IMR_TIMEOUT3		= (1 << 0),
};

enum rtl8187se_bits_config3 {
	BIT_CONFIG3_PARM_EN		= (1 << 6),
};

enum rtl8187se_bits_rate_fallback {
	BIT_RATE_FALLBACK_ENABLE	= (1 << 7),
	BIT_RATE_FALLBACK_AUTO_STEP0	= 0,
	BIT_RATE_FALLBACK_AUTO_STEP1	= 1,
	BIT_RATE_FALLBACK_AUTO_STEP2	= 2,
	BIT_RATE_FALLBACK_AUTO_STEP3	= 3,
};
