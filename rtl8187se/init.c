#include "rtl8187se.h"
#include "hw.h"
#include "rx.h"
#include "tx.h"
#include "irq.h"
#include "util.h"
#include "rf/rtl8225.h"
#include "../rtl818x.h"
#include <linux/etherdevice.h>

static int rtl8187se_init(struct rtl_priv *priv)
{
	struct ieee80211_hw *dev = priv->dev;
	struct rtl8187se_priv *p = kmalloc(sizeof(*p), GFP_KERNEL);
	struct eeprom_93cx6 *eeprom;
	u8 mac_addr[ETH_ALEN];
	int i;

	PRINT_INFO("Initializing hardware");

	if (!p)
		return -ENOMEM;

	priv->ops->priv = p;

	tasklet_init(&p->rx_ring.irq_tasklet, rtl8187se_irq_rx_tasklet, (unsigned long)priv);

	p->tx_conf = 0;
	p->tx_conf |= BIT_TX_CONF_HW_SEQNUM;
	p->tx_conf |= 7 << 21;	/* Max DMA burst size - 2048 bytes */
	p->tx_conf |= priv->retry_rts << 8;	/* RTS retry limit */
	p->tx_conf |= priv->retry_data << 0;	/* Data packet retry limit */

	p->rx_conf = 0;
	p->rx_conf |= BIT_RX_CONF_MGMT;
	p->rx_conf |= BIT_RX_CONF_DATA;
	p->rx_conf |= BIT_RX_CONF_CTRL;
	p->rx_conf |= BIT_RX_CONF_BROADCAST;
	p->rx_conf |= BIT_RX_CONF_MULTICAST;
	p->rx_conf |= BIT_RX_CONF_NICMAC;
	p->rx_conf |= 7 << 8;	/* Max DMA burst size - unlimited */
	p->rx_conf |= 7 << 13;	/* No RX threshold */
	p->rx_conf |= BIT_RX_CONF_ONLYERLPKT;

	PRINT_INFO("MAC controller is a RTL8187SE b/g");

	/* Read necessary info from EEPROM */
	eeprom = rtl_get_eeprom(priv);
	eeprom_93cx6_multiread(eeprom, 0x7, (__le16 *)mac_addr, 3);
	SET_IEEE80211_PERM_ADDR(dev, mac_addr);
	for (i = 0; i < 7; i++) {
		u16 word;
		eeprom_93cx6_read(eeprom, 0x30 + i, &word);
		priv->chtxpwr[i<<1] = word & 0xff;
		priv->chtxpwr[(i<<1)+1] = word >> 8;
	}
	for (i = 0; i < 7; i++) {
		u16 word;
		eeprom_93cx6_read(eeprom, 0x20 + i, &word);
		priv->chtxpwr_ofdm[i<<1] = word & 0xff;
		priv->chtxpwr_ofdm[(i<<1)+1] = word >> 8;
	}
	kfree(eeprom);

	p->rf = &rtl8225_rf_ops;

	/* Initialize descriptor rings */
	if (rtl8187se_init_rx_ring(priv, RTL8187SE_RX_BUFS, RTL8187SE_RX_BUF_SIZE))
		return -ENOMEM;
	for (i = 0; i < 6; i++) {
		if (rtl8187se_init_tx_ring(priv, 32, i))
			return -ENOMEM;
	}
	if (rtl8187se_init_tx_ring(priv, 2, 6))	/* beacon ring */
		return -ENOMEM;

	return 0;
}

static bool rtl8187se_reset(struct rtl_priv *priv)
{
	u8 reg;
	int timeout;

	rtl8187se_irq_disable(priv);

	// TODO: maybe only write?
	reg = rtl_ioread8(priv, REG_CMD);
	reg &= 2;	/* Clear all but reserved bit */
	reg |= BIT_CMD_RESET;
	rtl_iowrite8(priv, REG_CMD, reg);

	for (timeout = 200; timeout > 0; timeout--) {
		if (rtl_ioread8(priv, REG_CMD) & BIT_CMD_RESET)
			msleep(1);
		else
			break;
	}
	if (!timeout) {
		PRINT_ERR("NIC reset timeout");
		return false;
	} else
		PRINT_INFO("NIC reset success");

	/* Load registers from EEPROM - should take about 2 ms */
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_LOAD);

	for (timeout = 20; timeout > 0; timeout--) {
		if (rtl_get_eeprom_mode(priv) == RTL_EEPROM_MODE_NORMAL)
			return true;
		else
			msleep(1);
	}

	return false;
}

static void rtl8187se_clean(struct rtl_priv *priv)
{
	int i;

	PRINT_INFO("Removing hardware");

	rtl8187se_set_anaparam(priv, false);

	rtl8187se_reset(priv);
	mdelay(10);

	rtl8187se_free_rx_ring(priv);
	for (i = 0; i < 7; i++)
		rtl8187se_free_tx_ring(priv, i);

	kfree(priv->ops->priv);
}

static u8 mac_reg_table[][2] =	{
	/* PAGE 0: */
	{0x08, 0xae}, {0x0a, 0x72}, {0x5b, 0x42},
	{0x84, 0x88}, {0x85, 0x24}, {0x88, 0x54}, {0x8b, 0xb8}, {0x8c, 0x03},
	{0x8d, 0x40}, {0x8e, 0x00}, {0x8f, 0x00}, {0x5b, 0x18}, {0x91, 0x03},
	{0x94, 0x0F}, {0x95, 0x32},
	{0x96, 0x00}, {0x97, 0x07}, {0xb4, 0x22}, {0xdb, 0x00},
	{0xf0, 0x32}, {0xf1, 0x32}, {0xf2, 0x00}, {0xf3, 0x00}, {0xf4, 0x32},
	{0xf5, 0x43}, {0xf6, 0x00}, {0xf7, 0x00}, {0xf8, 0x46}, {0xf9, 0xa4},
	{0xfa, 0x00}, {0xfb, 0x00}, {0xfc, 0x96}, {0xfd, 0xa4}, {0xfe, 0x00},
	{0xff, 0x00},

	/* PAGE 1: */
	/* For Flextronics system Logo PCIHCT failure: */
	/* 0x1C4~0x1CD set no-zero value to avoid PCI configuration space 0x45[7]=1 */
	{0x5e, 0x01},
	{0x58, 0x00}, {0x59, 0x00}, {0x5a, 0x04}, {0x5b, 0x00}, {0x60, 0x24},
	{0x61, 0x97}, {0x62, 0xF0}, {0x63, 0x09}, {0x80, 0x0F}, {0x81, 0xFF},
	{0x82, 0xFF}, {0x83, 0x03},
	{0xC4, 0x22}, {0xC5, 0x22}, {0xC6, 0x22}, {0xC7, 0x22}, {0xC8, 0x22},
	{0xC9, 0x22}, {0xCA, 0x22}, {0xCB, 0x22}, {0xCC, 0x22}, {0xCD, 0x22},
	{0xe2, 0x00},

	/* PAGE 2: */
	{0x5e, 0x02},
	{0x0c, 0x04}, {0x4c, 0x30}, {0x4d, 0x08}, {0x50, 0x05}, {0x51, 0xf5},
	{0x52, 0x04}, {0x53, 0xa0}, {0x54, 0xff}, {0x55, 0xff}, {0x56, 0xff},
	{0x57, 0xff}, {0x58, 0x08}, {0x59, 0x08}, {0x5a, 0x08}, {0x5b, 0x08},
	{0x60, 0x08}, {0x61, 0x08}, {0x62, 0x08}, {0x63, 0x08}, {0x64, 0x2f},
	{0x8c, 0x3f}, {0x8d, 0x3f}, {0x8e, 0x3f},
	{0x8f, 0x3f}, {0xc4, 0xff}, {0xc5, 0xff}, {0xc6, 0xff}, {0xc7, 0xff},
	{0xc8, 0x00}, {0xc9, 0x00}, {0xca, 0x80}, {0xcb, 0x00},

	/* PAGE 0: */
	{0x5e, 0x00}, {0x9f, 0x03}
};

static void rtl8187se_config_mac(struct rtl_priv *priv)
{
	u16 index = 0;
	u8 reg;
	int i;

	for (i = 0; i < ARRAY_SIZE(mac_reg_table); i++) {
		u16 offset = mac_reg_table[i][0];
		u8 value = mac_reg_table[i][1];

		if (offset == 0x5e)
			index = value;
		else
			offset |= index << 8;

		rtl_iowrite8(priv, offset, (u8)value);
	}

	/* Follow TID_AC_MAP of WMac. */
	rtl_iowrite16(priv, REG_TID_AC_MAP, 0xfa50);

	/* Interrupt Migration */
	rtl_iowrite16(priv, REG_INT_MIG, 0x0000);

	/* Prevent TPC to cause CRC error */
	rtl_iowrite32(priv, 0x1F0, 0x00000000);
	rtl_iowrite32(priv, 0x1F4, 0x00000000);
	rtl_iowrite8(priv, 0x1F8, 0x00);

	/* Enable DA10 TX power saving */
	reg = rtl_ioread8(priv, REG_PHY_PARAM);
	rtl_iowrite8(priv, REG_PHY_PARAM, reg | (1<<2));

	/* POWER */
	rtl_iowrite16(priv, 0x360, 0x1000);
	rtl_iowrite16(priv, 0x362, 0x1000);

	/* AFE */
	rtl_iowrite16(priv, 0x370, 0x0560);
	rtl_iowrite16(priv, 0x372, 0x0560);
	rtl_iowrite16(priv, 0x374, 0x0DA4);
	rtl_iowrite16(priv, 0x376, 0x0DA4);
	rtl_iowrite16(priv, 0x378, 0x0560);
	rtl_iowrite16(priv, 0x37A, 0x0560);
	rtl_iowrite16(priv, 0x37C, 0x00EC);
	rtl_iowrite16(priv, 0x37E, 0x00EC);
	rtl_iowrite8(priv, 0x24E, 0x01);
}

static bool rtl8187se_start(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	u8 reg;

	PRINT_INFO("Starting");

	rtl_iowrite8(priv, 0x24e, 0x61);
	if (!rtl8187se_reset(priv))
		return false;

	/* Basic rate set register */
	rtl_iowrite16(priv, REG_BRSR, 0xffff);

	/* Retry limit */
	reg = rtl_ioread8(priv, REG_CW_CONF);
	rtl_iowrite8(priv, REG_CW_CONF, reg | 0x2);

	/* TX AGC */
	reg = rtl_ioread8(priv, REG_TX_AGC_CTL);
///	rtl_iowrite8(priv, REG_TX_GAIN_CCK, 128);
///	rtl_iowrite8(priv, REG_TX_GAIN_OFDM, 128);
///	rtl_iowrite8(priv, REG_TX_ANTENNA, 0);
	rtl_iowrite8(priv, REG_TX_AGC_CTL, reg & 0xf8);

	/* Automatic rate fallback register */
	rtl_iowrite16(priv, REG_ARFR, 0x0fff);	/* 1 Mbps - 54 Mbps */
	rtl_iowrite8(priv, REG_RATE_FALLBACK, BIT_RATE_FALLBACK_ENABLE | BIT_RATE_FALLBACK_AUTO_STEP1);

	/* Media status register */
	rtl_iowrite8(priv, REG_MSR, BIT_MSR_NO_LINK);

	/* Beacon interval */
	// FIXME: use usecs_to_jiffies
	rtl_iowrite16(priv, REG_BEACON_INTERVAL, priv->vif ? priv->vif->bss_conf.beacon_int : 100);

	/* Announcement traffic indication message window */
	rtl_iowrite16(priv, REG_ATIM_WND, 2);

	/* Function event mask register */
	rtl_iowrite16(priv, REG_FEMR, 0xffff);

	/* WPA config */
	rtl_iowrite8(priv, REG_WPA_CONF, 0);

	/* Configure Media Access Control layer */
	rtl8187se_config_mac(priv);

	rtl_iowrite16(priv, REG_RFSW_CTL, 0x569a);

	rtl8187se_set_anaparam(priv, true);

	/* Use LED1 to control HW RF on/off */
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	reg = rtl_ioread8(priv, REG_CONFIG5);
	rtl_iowrite8(priv, REG_CONFIG5, reg & ~(1<<3));
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);

	/* Enable LED */
	reg = rtl_ioread8(priv, REG_PGSELECT);
	rtl_iowrite8(priv, REG_PGSELECT, reg | (1<<3));

	/* Configure radio frontend */
	p->rf->start(priv);

	/* Set rings */
	rtl8187se_set_rx_ring(priv);
	rtl8187se_set_tx_rings(priv);

	/* Enable interrupts */
	rtl8187se_irq_enable(priv);

	/* Enable RX and TX */
	rtl8187se_rx_enable(priv);
	rtl8187se_tx_enable(priv);

	return true;
}

static void rtl8187se_stop(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;

	PRINT_INFO("Stopping");

	rtl8187se_rx_disable(priv);
	rtl8187se_tx_disable(priv);
	rtl8187se_irq_disable(priv);
	p->rf->stop(priv);
	rtl8187se_reset_rx_ring(priv);
	rtl8187se_reset_tx_rings(priv);
}

static bool rtl8187se_set_channel(struct rtl_priv *priv, short channel)
{
	struct rtl8187se_priv *p = priv->ops->priv;

	if (channel > 14 || channel < 1) {
		PRINT_ERR("Invalid channel %hd\n", channel);
		return false;
	}

	return p->rf->set_channel(priv, channel);
}

static void rtl8187se_bss_info_changed(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct ieee80211_vif *vif = priv->vif;
////	struct rtl_vif *vif_priv = (struct rtl_vif *)vif->drv_priv;
	u8 reg;
	bool up = false;

	// FIXME: implement features

	reg = rtl_ioread8(priv, REG_MSR);
	reg &= ~0x0c;

////	if (is_valid_ether_addr(info->bssid)) {
		up = true;
		switch (vif->type) {
		case NL80211_IFTYPE_ADHOC:
			reg |= BIT_MSR_ADHOC;
			break;
		case NL80211_IFTYPE_AP:
			reg |= BIT_MSR_MASTER;
			break;
		case NL80211_IFTYPE_STATION:
			reg |= BIT_MSR_INFRA;
			break;
		default:
			reg |= BIT_MSR_NO_LINK;
			up = false;
			break;
		}
////	} else
////		reg |= BIT_MSR_NO_LINK;

	rtl_iowrite8(priv, REG_MSR, reg);

	if (up)
		p->rx_conf |= BIT_RX_CONF_BSSID;
	else
		p->rx_conf &= ~BIT_RX_CONF_BSSID;
	rtl_iowrite32(priv, REG_RX_CONF, p->rx_conf);

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
////	rtl_iowrite32(priv, REG_BSSID, ((u32 *)net->bssid)[0]);
////	rtl_iowrite16(priv, REG_BSSID+4, ((u16 *)net->bssid)[2]);
	// FIXME: units of REG_BEACON_INTERVAL is 1024us - is it right?
////	rtl_iowrite16(priv, REG_BEACON_INTERVAL, info->beacon_interval);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

static void rtl8187se_start_beaconing(struct rtl_priv *priv)
{
	u16 reg;
////	reg = priv->vif->bss_conf.atim_window & 0x3ff;
////	rtl_iowrite16(priv, REG_ATIM_WND, reg);
	reg = priv->vif->bss_conf.beacon_int & 0x3ff;
	rtl_iowrite16(priv, REG_BEACON_INTERVAL, reg);
}

static void rtl8187se_stop_beaconing(struct rtl_priv *priv)
{
}

void rtl8187se_set_mac_address(struct rtl_priv *priv, u8 *addr)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iowrite32(priv, REG_MAC, ((u32 *)addr)[0]);
	rtl_iowrite16(priv, REG_MAC+4, ((u32 *)addr)[1] & 0xffff);
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

u64 rtl8187se_get_tsf(struct rtl_priv *priv)
{
	return rtl_ioread32(priv, REG_TSFT) | (u64)(rtl_ioread32(priv, REG_TSFT+4)) << 32;
}

struct rtl_ops rtl8187se_ops = {
	.init			= rtl8187se_init,
	.clean			= rtl8187se_clean,
	.start			= rtl8187se_start,
	.stop			= rtl8187se_stop,
	.rx_enable		= rtl8187se_rx_enable,
	.interrupt		= rtl8187se_interrupt,
	.set_channel		= rtl8187se_set_channel,
	.tx			= rtl8187se_tx,
	.is_tx_queue_empty	= rtl8187se_is_tx_queue_empty,
	.bss_info_changed	= rtl8187se_bss_info_changed,
	.start_beaconing	= rtl8187se_start_beaconing,
	.stop_beaconing		= rtl8187se_stop_beaconing,
	.set_mac_address	= rtl8187se_set_mac_address,
	.get_tsf		= rtl8187se_get_tsf,
};
