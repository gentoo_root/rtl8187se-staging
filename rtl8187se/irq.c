#include "irq.h"
#include "hw.h"
#include "../rtl818x.h"

void rtl8187se_interrupt(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	u32 isr = rtl_ioread32(priv, REG_ISR);
	rtl_iowrite32(priv, REG_ISR, isr);

	if (!isr || isr == 0xffffffff)
		return;

	if (isr & BIT_IMR_TIMEOUT1)
		rtl_iowrite32(priv, REG_TIMER_INT, 0);
	if (isr & BIT_IMR_TB_OK)
		rtl8187se_beacon_isr(priv, 6, false);
	if (isr & BIT_IMR_TB_ERR)
		rtl8187se_beacon_isr(priv, 6, true);
	if (isr & BIT_IMR_TMG_OK)
		rtl8187se_tx_isr(priv, 0, false);
	if (isr & BIT_IMR_THP_ERR)
		rtl8187se_tx_isr(priv, 5, true);
	if (isr & BIT_IMR_THP_OK)
		rtl8187se_tx_isr(priv, 5, false);
	if (isr & BIT_IMR_TBK_ERR)
		rtl8187se_tx_isr(priv, 1, true);
	if (isr & BIT_IMR_TBE_ERR)
		rtl8187se_tx_isr(priv, 2, true);
	if (isr & BIT_IMR_TVO_ERR)
		rtl8187se_tx_isr(priv, 4, true);
	if (isr & BIT_IMR_TVI_ERR)
		rtl8187se_tx_isr(priv, 3, true);
	if (isr & (BIT_IMR_RX_OK | BIT_IMR_RQOS_OK))	// FIXME: BIT_IMR_RX_ERR needed?
		tasklet_schedule(&p->rx_ring.irq_tasklet);
	if (isr & BIT_IMR_BEACON_TIMEOUT) {
		u16 reg = priv->vif ? cpu_to_le16(priv->vif->bss_conf.beacon_int) & 0x3ff : 100;
		rtl_iowrite16(priv, REG_BEACON_INTERVAL, reg);
		rtl_prepare_beacon(priv);
	}
	if (isr & BIT_IMR_RX_UNAVAIL) {
		if (!priv->rdu) {
			PRINT_ERR("No RX descriptor available");
			priv->rdu = true;
		}
		tasklet_schedule(&p->rx_ring.irq_tasklet);
	}
	if (isr & BIT_IMR_RX_OVERFLOW)
		tasklet_schedule(&p->rx_ring.irq_tasklet);
	if (isr & BIT_IMR_TVO_OK)
		rtl8187se_tx_isr(priv, 4, false);
	if (isr & BIT_IMR_TVI_OK)
		rtl8187se_tx_isr(priv, 3, false);
	if (isr & BIT_IMR_TBK_OK)
		rtl8187se_tx_isr(priv, 1, false);
	if (isr & BIT_IMR_TBE_OK)
		rtl8187se_tx_isr(priv, 2, false);
}

void rtl8187se_irq_rx_tasklet(unsigned long data)
{
	struct rtl_priv *priv = (struct rtl_priv *)data;
	rtl8187se_rx(priv);
}

void rtl8187se_irq_enable(struct rtl_priv *priv)
{
	u32 int_mask = 0;

	int_mask |= BIT_IMR_TMG_OK;
	int_mask |= BIT_IMR_TB_ERR;
	int_mask |= BIT_IMR_TB_OK;
	int_mask |= BIT_IMR_THP_ERR;
	int_mask |= BIT_IMR_THP_OK;
	int_mask |= BIT_IMR_TVO_ERR;
	int_mask |= BIT_IMR_TVO_OK;
	int_mask |= BIT_IMR_TVI_ERR;
	int_mask |= BIT_IMR_TVI_OK;
	int_mask |= BIT_IMR_TBE_ERR;
	int_mask |= BIT_IMR_TBE_OK;
	int_mask |= BIT_IMR_TBK_ERR;
	int_mask |= BIT_IMR_TBK_OK;
	int_mask |= BIT_IMR_RX_UNAVAIL;
	int_mask |= BIT_IMR_RX_ERR;
	int_mask |= BIT_IMR_RX_OK;
	int_mask |= BIT_IMR_RQOS_OK;

	rtl_iowrite32(priv, REG_IMR, int_mask);
	priv->irq_enabled = true;
}

void rtl8187se_irq_disable(struct rtl_priv *priv)
{
	priv->irq_enabled = false;
	rtl_iowrite32(priv, REG_IMR, 0);
}
