#pragma once

#include "../dev.h"

void rtl8187se_interrupt(struct rtl_priv *priv);
void rtl8187se_irq_rx_tasklet(unsigned long data);
void rtl8187se_irq_enable(struct rtl_priv *priv);
void rtl8187se_irq_disable(struct rtl_priv *priv);
