#pragma once

#include "../../dev.h"

/* Different radio frontends support */

struct rtl8187se_rf_ops {
	bool (*start)(struct rtl_priv *priv);
	void (*stop)(struct rtl_priv *priv);
	bool (*set_channel)(struct rtl_priv *priv, short channel);
};
