#include "rtl8225.h"
#include "../../rtl818x.h"
#include <linux/delay.h>
#include "../hw.h"	// TODO: remove it

enum rtl8225_regs {
	/* Radio frontend software config */
	REG_RF_SW_CONFIG	= 0x08,
	/* PHY address register */
	REG_PHY_ADDR		= 0x7c,
	/* PHY data register */
	REG_PHY_DATA		= 0x7e,
	/* Radio frontend pins */
	REG_RF_PINS_OUTPUT	= 0x80,
	REG_RF_PINS_ENABLE	= 0x82,
	REG_RF_PINS_SELECT	= 0x84,
	REG_RF_PINS_INPUT	= 0x86,
	/* Software 3-wire data buffer bits 0~31 */
	REG_SW_3W_DB0		= 0x274,
	/* Software 3-wire data buffer bits 32~63 */
	REG_SW_3W_DB1		= 0x278,
	/* Software 3-wire control/status register */
	REG_SW_3W_CMD1		= 0x27d,
	/* Serial Interface Data Register */
	REG_SI_DATA_READ	= 0x362,
};

enum rtl8225_bits_sw_3w_cmd1 {
	/* Read enable */
	BIT_SW_3W_CMD1_RE	= (1 << 0),
	/* Write enable */
	BIT_SW_3W_CMD1_WE	= (1 << 1),
	/* Operation done */
	BIT_SW_3W_CMD1_DONE	= (1 << 2),
};

enum rtl8225_bits_rf_sw_config {
	/* Enable serial interface */
	BIT_RF_SW_CONFIG_SERIAL	= (1 << 1),
};

bool rtl8225_write_phy(struct rtl_priv *priv, u32 data)
{
	int timeout;

	/* Wait for command bit to be clear */
	for (timeout = 30; timeout > 0; timeout--) {
		u8 reg = rtl_ioread8(priv, REG_PHY_ADDR);
		if (reg & 0x80)
			msleep(10);
		else
			break;
	}
	if (!timeout) {
		PRINT_ERR("PHY register timeout");
		return false;
	}

	/* We must write 0x7c after 0x7d-0x7f to write BB register */
	rtl_iowrite8(priv, REG_PHY_ADDR+3, (data >> 24) & 0xff);
	rtl_iowrite8(priv, REG_PHY_ADDR+2, (data >> 16) & 0xff);
	rtl_iowrite8(priv, REG_PHY_ADDR+1, (data >> 8) & 0xff);
	rtl_iowrite8(priv, REG_PHY_ADDR, data & 0xff);

	return true;
}

bool rtl8225_write_phy_ofdm(struct rtl_priv *priv, u8 addr, u8 data)
{
	u32 reg = data;
	reg = (reg<<8) | addr | 0x80;
	return rtl8225_write_phy(priv, reg);
}

bool rtl8225_write_phy_cck(struct rtl_priv *priv, u8 addr, u8 data)
{
	u32 reg = data;
	reg = (reg<<8) | addr | 0x80 | 0x1000000;
	return rtl8225_write_phy(priv, reg);
}

/* 3-wire High Speed Serial Interface */
static bool rtl8225_hssi(struct rtl_priv *priv, u16 *data, bool write)
{
	int timeout;
	u8 reg;

	for (timeout = 5; timeout > 0; timeout--) {
		reg = rtl_ioread8(priv, REG_SW_3W_CMD1);
		if (reg & (BIT_SW_3W_CMD1_RE|BIT_SW_3W_CMD1_WE))
			udelay(10);
		else
			break;
	}
	if (!timeout) {
		PRINT_ERR("HSSI wait RE|WE timeout");
		return false;
	}

	/* Enable serial interface */
	reg = rtl_ioread8(priv, REG_RF_SW_CONFIG);
	rtl_iowrite8(priv, REG_RF_SW_CONFIG, reg | BIT_RF_SW_CONFIG_SERIAL);

	reg = rtl_ioread8(priv, REG_RF_PINS_SELECT);
	rtl_iowrite8(priv, REG_RF_PINS_SELECT, reg & ~(1<<3));

	rtl_iowrite16(priv, REG_SW_3W_DB0, *data);
	rtl_iowrite8(priv, REG_SW_3W_CMD1, write ? BIT_SW_3W_CMD1_WE : BIT_SW_3W_CMD1_RE);

	for (timeout = 5; timeout > 0; timeout--) {
		reg = rtl_ioread8(priv, REG_SW_3W_CMD1);
		if (reg & BIT_SW_3W_CMD1_DONE)
			break;
		else
			udelay(10);
	}
	if (!timeout) {
		PRINT_ERR("HSSI wait DONE timeout");
		return false;
	}

	rtl_iowrite8(priv, REG_SW_3W_CMD1, 0);

	if (!write) {
		*data = rtl_ioread16(priv, REG_SI_DATA_READ);
		*data &= 0xfff;
	}

	mdelay(1);

	return true;
}

bool rtl8225_write_rf(struct rtl_priv *priv, u8 offset, u16 data)
{
	u16 reg = data << 4 | (offset & 0xf);
	return rtl8225_hssi(priv, &reg, true);
}

bool rtl8225_read_rf(struct rtl_priv *priv, u8 offset, u16 *data)
{
	*data = offset & 0xf;
	return rtl8225_hssi(priv, data, false);
}

static u32 zebra_rf_rx_gain_table[] = {
	0x0096, 0x0076, 0x0056, 0x0036, 0x0016, 0x01f6, 0x01d6, 0x01b6,
	0x0196, 0x0176, 0x00F7, 0x00D7, 0x00B7, 0x0097, 0x0077, 0x0057,
	0x0037, 0x00FB, 0x00DB, 0x00BB, 0x00FF, 0x00E3, 0x00C3, 0x00A3,
	0x0083, 0x0063, 0x0043, 0x0023, 0x0003, 0x01E3, 0x01C3, 0x01A3,
	0x0183, 0x0163, 0x0143, 0x0123, 0x0103
};

static u8 zebra_agc[] = {
	0x7E, 0x7E, 0x7E, 0x7E, 0x7D, 0x7C, 0x7B, 0x7A, 0x79, 0x78, 0x77, 0x76, 0x75, 0x74, 0x73, 0x72,
	0x71, 0x70, 0x6F, 0x6E, 0x6D, 0x6C, 0x6B, 0x6A, 0x69, 0x68, 0x67, 0x66, 0x65, 0x64, 0x63, 0x62,
	0x48, 0x47, 0x46, 0x45, 0x44, 0x29, 0x28, 0x27, 0x26, 0x25, 0x24, 0x23, 0x22, 0x21, 0x08, 0x07,
	0x06, 0x05, 0x04, 0x03, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x15, 0x16,
	0x17, 0x17, 0x18, 0x18, 0x19, 0x1a, 0x1a, 0x1b, 0x1b, 0x1c, 0x1c, 0x1d, 0x1d, 0x1d, 0x1e, 0x1e,
	0x1f, 0x1f, 0x1f, 0x20, 0x20, 0x20, 0x20, 0x21, 0x21, 0x21, 0x22, 0x22, 0x22, 0x23, 0x23, 0x24,
	0x24, 0x25, 0x25, 0x25, 0x26, 0x26, 0x27, 0x27, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F, 0x2F
};

static u8 ofdm_config[]	= {
	/* OFDM reg0x06[7:0]=0xFF: Enable power saving mode in RX */
	/* OFDM reg0x3C[4]=1'b1: Enable RX power saving mode */
	/* ofdm 0x3a = 0x7b, (original: 0xfb) For ECS shielding room TP test */
	/* 0x00	*/
	0x10, 0x0F, 0x0A, 0x0C, 0x14, 0xFA, 0xFF, 0x50,
	0x00, 0x50, 0x00, 0x00, 0x00, 0x5C, 0x00, 0x00,
	/* 0x10	*/
	0x40, 0x00, 0x40, 0x00, 0x00, 0x00, 0xA8, 0x26,
	0x32, 0x33, 0x06, 0xA5, 0x6F, 0x55, 0xC8, 0xBB,
	/* 0x20	*/
	0x0A, 0xE1, 0x2C, 0x4A, 0x86, 0x83, 0x34, 0x00,
	0x4F, 0x24, 0x6F, 0xC2, 0x03, 0x40, 0x80, 0x00,
	/* 0x30	*/
	0xC0, 0xC1, 0x58, 0xF1, 0x00, 0xC4, 0x90, 0x3e,
	0xD8, 0x3C, 0x7B, 0x10
};

bool rtl8225_start(struct rtl_priv *priv)
{
	bool d_cut = false;
	u16 reg1, reg2;
	u8 reg;
	int i;
	
	PRINT_INFO("Starting RF");

	/* Setup initial timing for radio frontend */
	rtl_iowrite16(priv, REG_RF_PINS_OUTPUT, 0x0480);
	rtl_iowrite16(priv, REG_RF_PINS_ENABLE, 0x1bff);
	rtl_iowrite16(priv, REG_RF_PINS_SELECT, 0x2488);

	/* Switch to page 1 */
	rtl8225_write_rf(priv, 0x0, 0x13f);

	rtl8225_read_rf(priv, 0x8, &reg1);
	rtl8225_read_rf(priv, 0x9, &reg2);
	if (reg1 == 0x818 && reg2 == 0x70c) {
		d_cut = true;
		PRINT_INFO("Card type changed from C-cut to D-cut");
	}

	/* Page 0 */
	rtl8225_write_rf(priv, 0x0, 0x09f);
	rtl8225_write_rf(priv, 0x1, 0x6e0);
	rtl8225_write_rf(priv, 0x2, 0x04d);
	rtl8225_write_rf(priv, 0x3, 0x7f1);
	rtl8225_write_rf(priv, 0x4, 0x975);
	rtl8225_write_rf(priv, 0x5, 0xc72);
	rtl8225_write_rf(priv, 0x6, 0xae6);
	rtl8225_write_rf(priv, 0x7, 0x0ca);
	rtl8225_write_rf(priv, 0x8, 0xe1c);
	rtl8225_write_rf(priv, 0x9, 0x2f0);
	rtl8225_write_rf(priv, 0xa, 0x9d0);
	rtl8225_write_rf(priv, 0xb, 0x1ba);
	rtl8225_write_rf(priv, 0xc, 0x640);
	rtl8225_write_rf(priv, 0xd, 0x8df);
	rtl8225_write_rf(priv, 0xe, 0x020);
	rtl8225_write_rf(priv, 0xf, 0x990);

	/* Page 1 */
	rtl8225_write_rf(priv, 0x0, 0x13f);
	rtl8225_write_rf(priv, 0x3, 0x806);
	rtl8225_write_rf(priv, 0x4, 0x3a7);
	rtl8225_write_rf(priv, 0x5, 0x59b);
	rtl8225_write_rf(priv, 0x6, 0x081);
	rtl8225_write_rf(priv, 0x7, 0x1a0);
	rtl8225_write_rf(priv, 0xa, 0x001);
	rtl8225_write_rf(priv, 0xb, 0x418);
	rtl8225_write_rf(priv, 0xc, 0xfbe);
	rtl8225_write_rf(priv, 0xd, 0x008);
	/* RX LO buffer */
	rtl8225_write_rf(priv, 0xe, d_cut ? 0x807 : 0x806);
	rtl8225_write_rf(priv, 0xf, 0xacc);
	rtl8225_write_rf(priv, 0x0, 0x1d7);
	rtl8225_write_rf(priv, 0x3, 0xe00);
	rtl8225_write_rf(priv, 0x4, 0xe50);
	for (i = 0; i < ARRAY_SIZE(zebra_rf_rx_gain_table); i++) {
		rtl8225_write_rf(priv, 0x1, i);
		rtl8225_write_rf(priv, 0x2, zebra_rf_rx_gain_table[i]);
	}
	rtl8225_write_rf(priv, 0x5, 0x203);
	rtl8225_write_rf(priv, 0x6, 0x200);
	rtl8225_write_rf(priv, 0x0, 0x137); mdelay(10);
	rtl8225_write_rf(priv, 0xd, 0x008); mdelay(10);
	rtl8225_write_rf(priv, 0x0, 0x037); mdelay(10);
	rtl8225_write_rf(priv, 0x4, 0x160); mdelay(10);
	rtl8225_write_rf(priv, 0x7, 0x080); mdelay(10);
	rtl8225_write_rf(priv, 0x2, 0x88d); mdelay(220);
	rtl8225_write_rf(priv, 0x0, 0x137); mdelay(10);
	rtl8225_write_rf(priv, 0x7, 0x000);
	rtl8225_write_rf(priv, 0x7, 0x180);
	rtl8225_write_rf(priv, 0x7, 0x220);
	rtl8225_write_rf(priv, 0x7, 0x3e0);

	/* DAC calibration off */
	rtl8225_write_rf(priv, 0x6, 0x0c1);
	rtl8225_write_rf(priv, 0xa, 0x001);

	/* XIN=6, XOUT=6 */
	rtl8225_write_rf(priv, 0xf, 0xacc);

	/* Page 0, HSSI enable */
	rtl8225_write_rf(priv, 0x0, 0x0bf);
	/* RX BB calibration */
	rtl8225_write_rf(priv, 0xd, 0x8df);
	/* Temperature meter off */
	rtl8225_write_rf(priv, 0x2, 0x04d);
	rtl8225_write_rf(priv, 0x4, 0x975);
	mdelay(30);
	rtl8225_write_rf(priv, 0x0, 0x197);
	rtl8225_write_rf(priv, 0x5, 0x5ab);
	rtl8225_write_rf(priv, 0x0, 0x09f);
	rtl8225_write_rf(priv, 0x1, 0x000);
	rtl8225_write_rf(priv, 0x2, 0x000);

	/* Power save parameters */
	reg = rtl_ioread8(priv, 0x24e);
	rtl_iowrite8(priv, 0x24e, reg & ~0x60);
	/* bit 7: power saving for TX */
	/* bit 6: power saving for RX */
	rtl8225_write_phy_cck(priv, 0x00, 0xc8);
	/* bit 4: turn off channel estimation related circuits if not doing channel estimation */
	/* bit 3: turn off unused circuits before cca = 1 */
	/* bit 2: turn off cck's circuit if macrst = 0 */
	rtl8225_write_phy_cck(priv, 0x06, 0x1c);
	rtl8225_write_phy_cck(priv, 0x10, 0x78);
	rtl8225_write_phy_cck(priv, 0x2e, 0xd0);
	rtl8225_write_phy_cck(priv, 0x2f, 0x06);
	rtl8225_write_phy_cck(priv, 0x01, 0x46);

	/* Power control */
	// TODO: move it from here
	rtl_iowrite8(priv, REG_TX_GAIN_CCK, 0x10);
	rtl_iowrite8(priv, REG_TX_GAIN_OFDM, 0x1b);
	rtl_iowrite8(priv, REG_TX_ANTENNA, 0x03);

	rtl8225_write_phy_ofdm(priv, 0x00, 0x12);
	for (i = 0; i < ARRAY_SIZE(zebra_agc); i++) {
		rtl8225_write_phy_ofdm(priv, 0x0f, zebra_agc[i]);
		rtl8225_write_phy_ofdm(priv, 0x0e, i+0x80);
		rtl8225_write_phy_ofdm(priv, 0x0e, 0);
	}
	rtl8225_write_phy_ofdm(priv, 0x00, 0x10);

	for (i = 0; i < ARRAY_SIZE(ofdm_config); i++) {
		rtl8225_write_phy_ofdm(priv, i, ofdm_config[i]);
	}

	return true;
}

void rtl8225_stop(struct rtl_priv *priv)
{
	PRINT_INFO("Stopping RF");
	rtl8225_write_rf(priv, 0x4, 0x01f);
	// TODO: is it needed?
	mdelay(1);
}

void rtl8225_set_tx_power(struct rtl_priv *priv, short channel)
{
	u8 cck_power = priv->chtxpwr[channel-1];
	u8 ofdm_power = priv->chtxpwr_ofdm[channel-1];

	if (cck_power > 35)
		cck_power = 35;
	if (ofdm_power > 35)
		ofdm_power = 35;

	rtl_iowrite8(priv, REG_TX_GAIN_CCK, cck_power);
	mdelay(1);

	if (!priv->vif) {
		rtl8225_write_phy_ofdm(priv, 0x02, 0x42);
		rtl8225_write_phy_ofdm(priv, 0x05, 0x00);
		rtl8225_write_phy_ofdm(priv, 0x06, 0x40);
		rtl8225_write_phy_ofdm(priv, 0x07, 0x00);
		rtl8225_write_phy_ofdm(priv, 0x08, 0x40);
	}

	rtl_iowrite8(priv, REG_TX_GAIN_OFDM, ofdm_power);

	if (ofdm_power <= 11) {
		rtl8225_write_phy_ofdm(priv, 0x07, 0x5c);
		rtl8225_write_phy_ofdm(priv, 0x09, 0x5c);
	}

	if (ofdm_power <= 17) {
		rtl8225_write_phy_ofdm(priv, 0x07, 0x54);
		rtl8225_write_phy_ofdm(priv, 0x09, 0x54);
	} else {
		rtl8225_write_phy_ofdm(priv, 0x07, 0x50);
		rtl8225_write_phy_ofdm(priv, 0x09, 0x50);
	}

	mdelay(1);
}

static const u16 rtl8225_chan[] = {
	0x080, 0x100, 0x180, 0x200, 0x280, 0x300, 0x380,
	0x400, 0x480, 0x500, 0x580, 0x600, 0x680, 0x74A,
};

bool rtl8225_set_channel(struct rtl_priv *priv, short channel)
{
	int timeout = 30;
	u16 reg;

	rtl8225_set_tx_power(priv, channel);
	do {
		rtl8225_write_rf(priv, 0x7, rtl8225_chan[channel-1]);
		rtl8225_read_rf(priv, 0x7, &reg);
	} while ((reg & 0xf80) != (rtl8225_chan[channel-1] & 0xf80) && --timeout);

	return timeout;
}

const struct rtl8187se_rf_ops rtl8225_rf_ops = {
	.start		= rtl8225_start,
	.stop		= rtl8225_stop,
	.set_channel	= rtl8225_set_channel,
};
