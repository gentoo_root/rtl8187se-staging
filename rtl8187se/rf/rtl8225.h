#pragma once

#include "rf.h"

extern const struct rtl8187se_rf_ops rtl8225_rf_ops;

bool rtl8225_write_phy_ofdm(struct rtl_priv *priv, u8 addr, u8 data);
bool rtl8225_write_phy_cck(struct rtl_priv *priv, u8 addr, u8 data);
