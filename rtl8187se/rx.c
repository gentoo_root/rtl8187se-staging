#include "rx.h"
#include "hw.h"
#include "../rtl818x.h"

int rtl8187se_init_rx_ring(struct rtl_priv *priv, size_t entries, size_t bufsize)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_rx_ring *ring = &p->rx_ring;
	struct pci_dev *pdev = priv->pdev;
	size_t ringsize = sizeof(*ring->desc)*entries+256;
	int i;

	if (bufsize > 0xfff) {
		PRINT_ERR("RX buffer is too large");
		return -ENOMEM;
	}

	ring->desc = pci_alloc_consistent(pdev, ringsize, &ring->dma);
	memset(ring->desc, 0, ringsize);
	ring->entries = entries;
	ring->idx = 0;
	ring->bufsize = bufsize;
	ring->skb_complete = true;

	if (ring->dma & 0xff) {
		PRINT_ERR("RX descriptors DMA buffer is not aligned");
		pci_free_consistent(pdev, ringsize, ring->desc, ring->dma);
		return -ENOMEM;
	}

	for (i = 0; i < entries; i++) {
		struct sk_buff *skb = dev_alloc_skb(bufsize);
		dma_addr_t *mapping;

		if (!skb) {
			PRINT_ERR("Failed to allocate RX buffer");
			return -ENOMEM;
		}

		ring->buf[i] = skb;
		mapping = (dma_addr_t *)skb->cb;
		*mapping = pci_map_single(pdev, skb_tail_pointer(skb), bufsize, PCI_DMA_FROMDEVICE);

		ring->desc[i].flags = cpu_to_le32(BIT_RX_DESC_OWN | bufsize);
		ring->desc[i].rx_buf = *mapping;
	}
	/* End of ring */
	ring->desc[entries-1].flags |= cpu_to_le32(BIT_RX_DESC_EOR);

	return 0;
}

void rtl8187se_free_rx_ring(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_rx_ring *ring = &p->rx_ring;
	struct pci_dev *pdev = priv->pdev;
	size_t ringsize = sizeof(*ring->desc)*ring->entries+256;
	int i;

	for (i = 0; i < ring->entries; i++) {
		struct sk_buff *skb = ring->buf[i];
		if (!skb)
			continue;
		pci_unmap_single(pdev, *((dma_addr_t *)skb->cb), ring->bufsize, PCI_DMA_FROMDEVICE);
		kfree_skb(skb);
	}
	pci_free_consistent(pdev, ringsize, ring->desc, ring->dma);
}

void rtl8187se_rx_enable(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	u8 reg;

	// FIXME: support monitor mode
////	if (priv->dev->flags & IFF_PROMISC ||
////	    priv->ieee80211->iw_mode == IW_MODE_MONITOR) {
////		p->rx_conf &= ~BIT_RX_CONF_NICMAC;
////		p->rx_conf |= BIT_RX_CONF_MONITOR;
////	} else {
		p->rx_conf |= BIT_RX_CONF_NICMAC;
		p->rx_conf &= ~BIT_RX_CONF_MONITOR;
////	}
////	if (priv->ieee80211->iw_mode == IW_MODE_MONITOR) {
////		p->rx_conf |= BIT_RX_CONF_CTRL;
////		p->rx_conf |= BIT_RX_CONF_PM;
////		p->rx_conf |= BIT_RX_CONF_ICV;
////		if (priv->crcmon)
////			p->rx_conf |= BIT_RX_CONF_FCS;
////		else
////			p->rx_conf &= ~BIT_RX_CONF_FCS;
////	} else {
		p->rx_conf &= ~BIT_RX_CONF_CTRL;
		p->rx_conf &= ~BIT_RX_CONF_PM;
		p->rx_conf &= ~BIT_RX_CONF_ICV;
		p->rx_conf &= ~BIT_RX_CONF_FCS;
////	}
	rtl_iowrite32(priv, REG_RX_CONF, p->rx_conf);

	reg = rtl_ioread8(priv, REG_CMD);
	rtl_iowrite8(priv, REG_CMD, reg | BIT_CMD_RX_ENABLE);
}

void rtl8187se_rx_disable(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	u8 reg = rtl_ioread8(priv, REG_CMD);

	rtl_iowrite8(priv, REG_CMD, reg & ~BIT_CMD_RX_ENABLE);

	if (!p->rx_ring.skb_complete)
		dev_kfree_skb_any(p->rx_ring.skb);
}

void rtl8187se_set_rx_ring(struct rtl_priv *priv)
{
	/* RX ring reset is needed */
	struct rtl8187se_priv *p = priv->ops->priv;
	u8 reg = rtl_ioread8(priv, REG_PGSELECT);
	rtl_iowrite8(priv, REG_PGSELECT, reg & ~BIT_PGSELECT_PAGE);
	rtl_iowrite32(priv, REG_RX_RING_ADDR, p->rx_ring.dma);
}

void rtl8187se_reset_rx_ring(struct rtl_priv *priv)
{
	/* RX have to be disabled */
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_rx_ring *ring = &p->rx_ring;
	int i;

	for (i = 0; i < ring->entries; i++) {
		ring->desc[i].rx_buf = cpu_to_le32(*((dma_addr_t *)ring->buf[i]->cb));
		ring->desc[i].flags = cpu_to_le32(BIT_RX_DESC_OWN | ring->bufsize);
	}
	ring->desc[ring->entries-1].flags |= cpu_to_le32(BIT_RX_DESC_EOR);

	ring->idx = 0;
	ring->skb_complete = true;
}

#define MAX_FRAG_THRESHOLD	2346U

void rtl8187se_rx(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct ieee80211_hw *dev = priv->dev;
	struct sk_buff *tmp_skb;
	bool first, last;
	u32 len;
	int lastlen;
	u8 quality, signal;
	u8 rate;
	char rxpower = 0;
	u32 RXAGC = 0;
	long RxAGC_dBm = 0;
	u8	LNA = 0, BB = 0;
	u8	LNA_gain[4] = {02, 17, 29, 39};

	/* while there are filled descriptors */
	while (1) {
		struct rtl8187se_rx_desc *entry = &p->rx_ring.desc[p->rx_ring.idx];
		u32 flags = le32_to_cpu(entry->flags);
		if (flags & BIT_RX_DESC_OWN)
			break;
		if (flags & BIT_RX_DESC_FOF)
			PRINT_WARN("RX buffer overflow");

		if (flags & BIT_RX_DESC_DMA_FAIL) {
			PRINT_ERR("RX DMA failed at buffer pointed by descriptor %d", p->rx_ring.idx);
			goto rtl8187se_rx_drop;
		}

		pci_dma_sync_single_for_cpu(priv->pdev,
			*((dma_addr_t *)p->rx_ring.buf[p->rx_ring.idx]->cb),
			p->rx_ring.bufsize, PCI_DMA_FROMDEVICE);

		first = flags & BIT_RX_DESC_FS;
		if (first)
			p->rx_ring.prevlen = 0;

		last = flags & BIT_RX_DESC_LS;
		if (last) {
			lastlen = flags & 0xfff;

			/* if the last descriptor (that should
			 * tell us the total packet len) tell
			 * us something less than the descriptors
			 * len we had until now, then there is some
			 * problem..
			 * workaround to prevent kernel panic
			 */
			if (lastlen < p->rx_ring.prevlen)
				len = 0;
			else
				len = lastlen-p->rx_ring.prevlen;
		} else {
			len = p->rx_ring.bufsize;
		}

		p->rx_ring.prevlen += len;

		if (p->rx_ring.prevlen > MAX_FRAG_THRESHOLD + 100) {
			/* HW is probably passing several buggy frames
			* without FD or LD flag set.
			* Throw this garbage away to prevent skb
			* memory exhausting
			*/
			if (!p->rx_ring.skb_complete)
				dev_kfree_skb_any(p->rx_ring.skb);
			p->rx_ring.skb_complete = true;
		}

		signal = entry->agc >> 1;
		quality = entry->sq;

		rxpower = (entry->rxpower)/2 - 42;

		rate = (flags >> 20) & 0xf;

		if (!rtl_is_b_mode(rtl_hw_value_to_rate(rate))) { /* OFDM rate. */
			RxAGC_dBm = rxpower+1;	/* bias */
		} else { /* CCK rate. */
			RxAGC_dBm = signal; /* bit 0 discard */

			LNA = (RxAGC_dBm & 0x60) >> 5; /* bit 6~ bit 5 */
			BB  = (RxAGC_dBm & 0x1F); /* bit 4 ~ bit 0 */

			RxAGC_dBm = -(LNA_gain[LNA] + (BB*2)); /* Pin_11b=-(LNA_gain+BB_gain) (dBm) */

			RxAGC_dBm += 4; /* bias */
		}

		if (RxAGC_dBm & 0x80) /* absolute value */
			RXAGC = ~(RxAGC_dBm)+1;
		/* Translate RXAGC into 1-100. */
		if (!rtl_is_b_mode(rtl_hw_value_to_rate(rate))) { /* OFDM rate. */
			if (RXAGC > 90)
				RXAGC = 90;
			else if (RXAGC < 25)
				RXAGC = 25;
			RXAGC = (90-RXAGC)*100/65;
		} else { /* CCK rate. */
			if (RXAGC > 95)
				RXAGC = 95;
			else if (RXAGC < 30)
				RXAGC = 30;
			RXAGC = (95-RXAGC)*100/65;
		}
		/* SQ translation formula is provided by SD3 DZ. 2006.06.27 */
		if (quality >= 127)
			quality = 1; /*0; */ /* 0 will cause epc to show signal zero , walk around now; */
		else if (quality < 27)
			quality = 100;
		else
			quality = 127 - quality;

		if (first) {
			if (!p->rx_ring.skb_complete) {
				/* seems that HW sometimes fails to receive and
				   doesn't provide the last descriptor */
				dev_kfree_skb_any(p->rx_ring.skb);
			}
			p->rx_ring.skb = dev_alloc_skb(len+2);
			if (!p->rx_ring.skb)
				goto rtl8187se_rx_drop;

			p->rx_ring.skb_complete = false;
////			p->rx_ring.skb->dev = dev;
		} else {
			/* if we are here we should have already RXed
			* the first frame.
			* If we get here and the skb is not allocated then
			* we have just throw out garbage (skb not allocated)
			* and we are still rxing garbage....
			*/
			if (!p->rx_ring.skb_complete) {
				tmp_skb = dev_alloc_skb(p->rx_ring.skb->len+len+2);

				if (!tmp_skb)
					goto rtl8187se_rx_drop;

////				tmp_skb->dev = dev;

				memcpy(skb_put(tmp_skb, p->rx_ring.skb->len),
					p->rx_ring.skb->data,
					p->rx_ring.skb->len);

				dev_kfree_skb_any(p->rx_ring.skb);

				p->rx_ring.skb = tmp_skb;
			}
		}

		if (!p->rx_ring.skb_complete)
			memcpy(skb_put(p->rx_ring.skb, len), p->rx_ring.buf[p->rx_ring.idx]->data, len);

		if (last && !p->rx_ring.skb_complete) {
			if (p->rx_ring.skb->len > 4)
				skb_trim(p->rx_ring.skb, p->rx_ring.skb->len-4);
			ieee80211_rx_irqsafe(dev, p->rx_ring.skb);
			p->rx_ring.skb_complete = true;
		}

		pci_dma_sync_single_for_device(priv->pdev,
				    *((dma_addr_t *)p->rx_ring.buf[p->rx_ring.idx]->cb),
				    p->rx_ring.bufsize,
				    PCI_DMA_FROMDEVICE);

rtl8187se_rx_drop: /* this is used when we have not enough mem */
		/* restore the descriptor */
		entry->rx_buf = cpu_to_le32(*((dma_addr_t *)p->rx_ring.buf[p->rx_ring.idx]->cb));
		entry->flags &= cpu_to_le32(~0xfff);
		entry->flags |= cpu_to_le32(p->rx_ring.bufsize);

		entry->flags |= cpu_to_le32(BIT_RX_DESC_OWN);

		p->rx_ring.idx = (p->rx_ring.idx+1) % p->rx_ring.entries;
	}
}
