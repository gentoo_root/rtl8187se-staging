#pragma once

#include "../dev.h"

enum rtl8187se_rx_desc_flags {
	/* Integrity check value error */
	BIT_RX_DESC_ICV_ERR		= (1 << 12),
	/* CRC32 error */
	BIT_RX_DESC_CRC32_ERR		= (1 << 13),
	/* Power management frame */
	BIT_RX_DESC_PM			= (1 << 14),
	/* Receive error */
	BIT_RX_DESC_RX_ERR		= (1 << 15),
	/* Broadcast frame */
	BIT_RX_DESC_BCAST		= (1 << 16),
	BIT_RX_DESC_PAM			= (1 << 17),
	/* Multicast frame */
	BIT_RX_DESC_MCAST		= (1 << 18),
	/* Quality of Service frame */
	BIT_RX_DESC_QOS			= (1 << 19),
	BIT_RX_DESC_TRSW		= (1 << 24),
	BIT_RX_DESC_SPLCP		= (1 << 25),
	/* FIFO overflow */
	BIT_RX_DESC_FOF			= (1 << 26),
	/* DMA failure */
	BIT_RX_DESC_DMA_FAIL		= (1 << 27),
	/* Last fragment */
	BIT_RX_DESC_LS			= (1 << 28),
	/* First fragment */
	BIT_RX_DESC_FS			= (1 << 29),
	/* End of ring */
	BIT_RX_DESC_EOR			= (1 << 30),
	/* Device owns descriptor */
	BIT_RX_DESC_OWN			= (1 << 31)
};

enum rtl8187se_bits_rx_conf {
	BIT_RX_CONF_MONITOR		= (1 <<  0),
	BIT_RX_CONF_NICMAC		= (1 <<  1),
	BIT_RX_CONF_MULTICAST		= (1 <<  2),
	BIT_RX_CONF_BROADCAST		= (1 <<  3),
	BIT_RX_CONF_FCS			= (1 <<  5),
	BIT_RX_CONF_ICV			= (1 << 12),
	BIT_RX_CONF_DATA		= (1 << 18),
	BIT_RX_CONF_CTRL		= (1 << 19),
	BIT_RX_CONF_MGMT		= (1 << 20),
	BIT_RX_CONF_ADDR3		= (1 << 21),
	BIT_RX_CONF_PM			= (1 << 22),
	BIT_RX_CONF_BSSID		= (1 << 23),
	BIT_RX_CONF_RX_AUTORESETPHY	= (1 << 28),
	BIT_RX_CONF_CSDM1		= (1 << 29),
	BIT_RX_CONF_CSDM2		= (1 << 30),
	BIT_RX_CONF_ONLYERLPKT		= (1 << 31),
};

struct rtl8187se_rx_desc {
	__le32 flags;
	union {
		__le64 mac_time;
		struct {
			u32 reserved1;
			__le32 rx_buf;
		} __packed;
	};
	u8 sq;
	u8 rssi;
	u8 agc;
	u8 flags2;
	u8 unknown1;
	u8 unknown2;
	u8 rxpower;
	u8 unknown3;
	u32 reserved2[3];
} __packed;

#define RTL8187SE_RX_BUFS	64
#define RTL8187SE_RX_BUF_SIZE	2353

struct rtl8187se_rx_ring {
	struct rtl8187se_rx_desc *desc;
	size_t entries;
	size_t idx;
	dma_addr_t dma;

	struct sk_buff *buf[RTL8187SE_RX_BUFS];
	size_t bufsize;

	struct sk_buff *skb;
	bool skb_complete;
	size_t prevlen;
	struct tasklet_struct irq_tasklet;
};

int rtl8187se_init_rx_ring(struct rtl_priv *priv, size_t entries, size_t bufsize);
void rtl8187se_free_rx_ring(struct rtl_priv *priv);
void rtl8187se_rx_enable(struct rtl_priv *priv);
void rtl8187se_rx_disable(struct rtl_priv *priv);
void rtl8187se_set_rx_ring(struct rtl_priv *priv);
void rtl8187se_reset_rx_ring(struct rtl_priv *priv);
void rtl8187se_rx(struct rtl_priv *priv);
