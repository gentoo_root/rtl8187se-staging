#include "tx.h"
#include "hw.h"
#include "../rtl818x.h"
#include <linux/etherdevice.h>

int rtl8187se_init_tx_ring(struct rtl_priv *priv, size_t entries, int priority)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_tx_ring *ring = &p->tx_ring[priority];
	struct pci_dev *pdev = priv->pdev;
	size_t ringsize = sizeof(*ring->desc)*entries+256;
	int i;

	ring->desc = pci_alloc_consistent(pdev, ringsize, &ring->dma);
	memset(ring->desc, 0, ringsize);
	ring->entries = entries;
	ring->idx = 0;
	ring->head_idx = 0;

	if (ring->dma & 0xff) {
		PRINT_ERR("TX descriptors DMA buffer is not aligned");
		pci_free_consistent(pdev, ringsize, ring->desc, ring->dma);
		return -ENOMEM;
	}

	for (i = 0; i < entries; i++) {
		ring->desc[i].next_tx_desc = cpu_to_le32((u32)ring->dma + ((i+1)%entries)*sizeof(*ring->desc));
	}
	skb_queue_head_init(&ring->queue);

	return 0;
}

void rtl8187se_free_tx_ring(struct rtl_priv *priv, int priority)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_tx_ring *ring = &p->tx_ring[priority];
	struct pci_dev *pdev = priv->pdev;
	size_t ringsize = sizeof(*ring->desc)*ring->entries+256;

	while (!skb_queue_empty(&ring->queue)) {
		struct rtl8187se_tx_desc *desc = &ring->desc[ring->head_idx];
		struct sk_buff *skb = skb_dequeue(&ring->queue);

		pci_unmap_single(pdev, le32_to_cpu(desc->tx_buf), skb->len, PCI_DMA_TODEVICE);
		kfree_skb(skb);
		ring->head_idx = (ring->head_idx+1) % ring->entries;
	}
	pci_free_consistent(pdev, ringsize, ring->desc, ring->dma);
}

void rtl8187se_tx_enable(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	u8 reg;

	rtl_iowrite32(priv, REG_TX_CONF, p->tx_conf);

	reg = rtl_ioread8(priv, REG_MSR);
	rtl_iowrite8(priv, REG_MSR, reg | BIT_MSR_ENEDCA);

	reg = rtl_ioread8(priv, REG_CMD);
	rtl_iowrite8(priv, REG_CMD, reg | BIT_CMD_TX_ENABLE);
}

void rtl8187se_tx_disable(struct rtl_priv *priv)
{
	u8 reg = rtl_ioread8(priv, REG_CMD);
	rtl_iowrite8(priv, REG_CMD, reg & ~BIT_CMD_TX_ENABLE);
}

static const uintptr_t rtl8187se_tx_ring_addrs[] = {
	REG_TX_MG_RING_ADDR,
	REG_TX_BK_RING_ADDR,
	REG_TX_BE_RING_ADDR,
	REG_TX_VI_RING_ADDR,
	REG_TX_VO_RING_ADDR,
	REG_TX_HI_RING_ADDR,
	REG_TX_BC_RING_ADDR,
};

void rtl8187se_set_tx_rings(struct rtl_priv *priv)
{
	/* TX reset is needed */
	struct rtl8187se_priv *p = priv->ops->priv;
	int i;

	for (i = 0; i < 7; i++)
		rtl_iowrite32(priv, rtl8187se_tx_ring_addrs[i], p->tx_ring[i].dma);
}

void rtl8187se_reset_tx_rings(struct rtl_priv *priv)
{
	/* TX have to be disabled */
	struct rtl8187se_priv *p = priv->ops->priv;
	int prio;

	for (prio = 0; prio < 7; prio++) {
		struct rtl8187se_tx_ring *ring = &p->tx_ring[prio];
		int i;

		for (i = 0; i < ring->entries; i++)
			ring->desc[i].flags &= cpu_to_le32(BIT_TX_DESC_OWN);

		ring->idx = 0;
		ring->head_idx = 0;
	}

	priv->ack_tx_to_ieee = false;
}

static void rtl8187se_dma_kick(struct rtl_priv *priv, int priority)
{
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);
	rtl_iowrite8(priv, REG_TX_DMA_POLLING, 1 << (priority + 1));
	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}

bool rtl8187se_is_tx_queue_empty(struct rtl_priv *priv)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	size_t idx;
	int i;

	for (i = 0; i < 6; i++) {
		for (idx = 0; idx < p->tx_ring[i].entries; idx++)
			if (p->tx_ring[i].desc[idx].flags & BIT_TX_DESC_OWN)
				return false;
	}
	return true;
}

/*
 * This function do the real dirty work: it enqueues a TX command
 * descriptor in the ring buffer, copyes the frame in a TX buffer
 * and kicks the NIC to ensure it does the DMA transfer.
 */
int rtl8187se_tx(struct rtl_priv *priv, struct sk_buff *skb, int priority, bool morefrag, int rate)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_tx_desc *tail;
	int count;
	struct rtl_tx_param tx_param;
	size_t idx = p->tx_ring[priority].idx;
	dma_addr_t mapping;

	rtl_get_tx_param(&tx_param, priv, skb, rate);

	tail = &p->tx_ring[priority].desc[idx];
	count = p->tx_ring[priority].entries;

	if (tail->flags & cpu_to_le32(1<<31)) {
		if (priority != 6) {
			/* It's OK to drop beacons */
			PRINT_WARN("No more TX descriptors in ring %d", priority);
		}
		return skb->len;
	}

	tail->flags = 0; /* zeroes header */
	tail->rts_duration = 0;
	tail->len = 0;
	tail->frame_len = 0;
	tail->tx_duration = 0;
	tail->retry = 0;
	tail->reserved[0] = 0;
	tail->reserved[1] = 0;

	/* FIXME: this should be triggered by HW encryption parameters.*/
	tail->flags |= cpu_to_le32(1<<15); /* no encrypt */

	tail->flags |= cpu_to_le32(1<<29); /* first segment of the packet */
	tail->flags |= cpu_to_le32(skb->len);

	mapping = pci_map_single(priv->pdev, skb->data, skb->len, PCI_DMA_TODEVICE);
	tail->tx_buf = cpu_to_le32(mapping);

	tail->frame_len = cpu_to_le16(skb->len & 0xfff); /* buffer length */

	if (tx_param.rts_enable) {
		tail->flags |= cpu_to_le32(rtl_rate_to_hw_value(rate)<<19); /* RTS RATE */
		tail->flags |= cpu_to_le32(1<<23); /* rts enable */
		tail->rts_duration |= cpu_to_le16(tx_param.rts_duration); /* RTS Duration */
	}
	tail->tx_duration = cpu_to_le16(tx_param.tx_duration); /* DURATION */
	tail->retry |= cpu_to_le32(11<<8); /* retry lim; */

	tail->flags |= cpu_to_le32((rtl_rate_to_hw_value(rate)&0xf) << 24);

	if (morefrag)
		tail->flags |= cpu_to_le32(1<<17); /* more fragment */
	tail->flags |= cpu_to_le32(1<<28); /* last segment of frame */

	idx = (idx+1) % count;

	if (priority != 6) {
		p->tx_ring[priority].idx = idx;
		/*
		 * BEACON_PRIORITY:
		 * The HW seems to be happy with the 1st
		 * descriptor filled and the 2nd empty...
		 * So always update descriptor 1 and never
		 * touch 2nd
		 */
	}

	skb_queue_tail(&p->tx_ring[priority].queue, skb);
	if (priority != 6 && count - skb_queue_len(&p->tx_ring[priority].queue) < 2)
		ieee80211_stop_queue(priv->dev, priority);
	tail->flags |= cpu_to_le32(1<<31); /* descriptor ready to be txed */
	rtl8187se_dma_kick(priv, priority);

	return 0;
}

void rtl8187se_tx_isr(struct rtl_priv *priv, int pri, bool error)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	size_t idx;
	size_t head_idx;
	struct rtl8187se_tx_desc *desc; /* start of ring virtual addr */
	size_t nic_idx; /* nic pointer */
	dma_addr_t nic; /* nic pointer physical addr */
	dma_addr_t nicbegin; /* start of ring physical addr */
	unsigned long flags;
	/* physical addr are ok on 32 bits since we set DMA mask */
	int j, i;
	size_t entries = p->tx_ring[pri].entries;

	spin_lock_irqsave(&priv->tx_lock, flags);
	
	idx = p->tx_ring[pri].idx;
	desc = p->tx_ring[pri].desc;
	head_idx = p->tx_ring[pri].head_idx;
	nic = rtl_ioread32(priv, rtl8187se_tx_ring_addrs[pri]);
	nicbegin = p->tx_ring[pri].dma;

	nic_idx = (nic-nicbegin) / sizeof(*desc);
	if ((head_idx <= idx && (nic_idx > idx || nic_idx < head_idx)) ||
	    (head_idx >  idx && (nic_idx > idx && nic_idx < head_idx))) {
		PRINT_WARN("NIC has lost pointer in ring %d - points to %u", pri, nic_idx);
		spin_unlock_irqrestore(&priv->tx_lock, flags);
		// FIXME: reset device
////		rtl_restart(priv->dev);
		return;
	}

	/*
	 * We check all the descriptors between the head and the nic,
	 * but not the currently pointed by the nic (the next to be txed)
	 * and the previous of the pointed (might be in process ??)
	 */

	j = nic_idx - head_idx;
	if (j < 0)
		j += entries-1;

	j -= 2;

	for (i = 0; i < j; i++) {
		struct sk_buff *skb;

		if (desc[head_idx].flags & cpu_to_le32(1<<31))
			break;

		skb = skb_dequeue(&p->tx_ring[pri].queue);
		pci_unmap_single(priv->pdev, le32_to_cpu(desc[head_idx].tx_buf), skb->len, PCI_DMA_TODEVICE);
		dev_kfree_skb_any(skb);

		head_idx = (head_idx + 1) % entries;

		if (entries - skb_queue_len(&p->tx_ring[pri].queue) == 2)
			ieee80211_wake_queue(priv->dev, pri);
	}

	/*
	 * The head has been moved to the last certainly TXed
	 * (or at least processed by the nic) packet.
	 * The driver take forcefully owning of all these packets
	 * If the packet previous of the nic pointer has been
	 * processed this doesn't matter: it will be checked
	 * here at the next round. Anyway if no more packet are
	 * TXed no memory leak occur at all.
	 */

	p->tx_ring[pri].head_idx = head_idx;
	if (!pri) {	/* Manage priority */
		if (priv->ack_tx_to_ieee) {
			if (rtl8187se_is_tx_queue_empty(priv)) {
				priv->ack_tx_to_ieee = false;
////				ieee80211_ps_tx_ack(priv->ieee80211, !error);
			}
		}
	}

	spin_unlock_irqrestore(&priv->tx_lock, flags);
}

void rtl8187se_beacon_isr(struct rtl_priv *priv, int pri, bool error)
{
	struct rtl8187se_priv *p = priv->ops->priv;
	struct rtl8187se_tx_desc *desc = p->tx_ring[pri].desc;
	struct sk_buff *skb;
	unsigned long flags;

	spin_lock_irqsave(&priv->tx_lock, flags);

	desc[0].flags &= ~cpu_to_le32(BIT_TX_DESC_OWN);
	skb = skb_dequeue(&p->tx_ring[pri].queue);
	pci_unmap_single(priv->pdev, le32_to_cpu(desc[0].tx_buf), skb->len, PCI_DMA_TODEVICE);
	dev_kfree_skb_any(skb);

	spin_unlock_irqrestore(&priv->tx_lock, flags);
}
