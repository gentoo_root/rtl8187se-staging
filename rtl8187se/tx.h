#pragma once

#include "../dev.h"

enum rtl8187se_tx_desc_flags {
	BIT_TX_DESC_NO_ENC		= (1 << 15),
	BIT_TX_DESC_TX_OK		= (1 << 15),
	BIT_TX_DESC_SPLCP		= (1 << 16),
	BIT_TX_DESC_RX_UNDER		= (1 << 16),
	BIT_TX_DESC_MOREFRAG		= (1 << 17),
	BIT_TX_DESC_CTS			= (1 << 18),
	BIT_TX_DESC_RTS			= (1 << 23),
	BIT_TX_DESC_LS			= (1 << 28),
	BIT_TX_DESC_FS			= (1 << 29),
	BIT_TX_DESC_DMA			= (1 << 30),
	BIT_TX_DESC_OWN			= (1 << 31)
};

enum rtl8187se_bits_tx_conf {
	BIT_TX_CONF_LOOPBACK_MAC	= (1 << 17),
	BIT_TX_CONF_LOOPBACK_CONT	= (3 << 17),
	BIT_TX_CONF_NO_ICV		= (1 << 19),
	BIT_TX_CONF_DISCW		= (1 << 20),
	BIT_TX_CONF_SAT_HWPLCP		= (1 << 24),
	BIT_TX_CONF_R8180_ABCD		= (2 << 25),
	BIT_TX_CONF_R8180_F		= (3 << 25),
	BIT_TX_CONF_R8185_ABC		= (4 << 25),
	BIT_TX_CONF_R8185_D		= (5 << 25),
	BIT_TX_CONF_R8187vD		= (5 << 25),
	BIT_TX_CONF_R8187vD_B		= (6 << 25),
	BIT_TX_CONF_HWVER_MASK		= (7 << 25),
	BIT_TX_CONF_DISREQQSIZE		= (1 << 28),
	BIT_TX_CONF_PROBE_DTS		= (1 << 29),
	BIT_TX_CONF_HW_SEQNUM		= (1 << 30),
	BIT_TX_CONF_CW_MIN		= (1 << 31),
};

struct rtl8187se_tx_desc {
	__le32 flags;
	__le16 rts_duration;
	__le16 len;
	__le32 tx_buf;
	__le16 frame_len;
	__le16 tx_duration;
	__le32 next_tx_desc;
	__le32 retry;
	__le32 reserved[2];
} __packed;

struct rtl8187se_tx_ring {
	struct rtl8187se_tx_desc *desc;
	size_t idx;
	size_t head_idx;
	size_t entries;
	dma_addr_t dma;

	struct sk_buff_head queue;
};

int rtl8187se_init_tx_ring(struct rtl_priv *priv, size_t entries, int priority);
void rtl8187se_free_tx_ring(struct rtl_priv *priv, int priority);
void rtl8187se_tx_enable(struct rtl_priv *priv);
void rtl8187se_tx_disable(struct rtl_priv *priv);
void rtl8187se_set_tx_rings(struct rtl_priv *priv);
void rtl8187se_reset_tx_rings(struct rtl_priv *priv);
bool rtl8187se_is_tx_queue_empty(struct rtl_priv *priv);
int rtl8187se_tx(struct rtl_priv *priv, struct sk_buff *skb, int priority, bool morefrag, int rate);
void rtl8187se_tx_isr(struct rtl_priv *priv, int pri, bool error);
void rtl8187se_beacon_isr(struct rtl_priv *priv, int pri, bool error);
