#include "util.h"
#include "hw.h"
#include "../rtl818x.h"

enum rtl8187se_anaparam {
	RTL8187SE_ANAPARAM_ON	= 0xB0054D00,
	RTL8187SE_ANAPARAM_OFF	= 0x55480658,
	RTL8187SE_ANAPARAM2_ON	= 0x000004C6,
	RTL8187SE_ANAPARAM2_OFF	= 0x72003f70,
	RTL8187SE_ANAPARAM3_ON	= 0x0010,
	RTL8187SE_ANAPARAM3_OFF	= 0x0000,
};

void rtl8187se_set_anaparam(struct rtl_priv *priv, bool state)
{
	u8 config3;

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_CONFIG);

	config3 = rtl_ioread8(priv, REG_CONFIG3);
	rtl_iowrite8(priv, REG_CONFIG3, config3 | BIT_CONFIG3_PARM_EN);

	rtl_iowrite32(priv, REG_ANAPARAM2, state ? RTL8187SE_ANAPARAM2_ON : RTL8187SE_ANAPARAM2_OFF);
	rtl_iowrite32(priv, REG_ANAPARAM, state ? RTL8187SE_ANAPARAM_ON : RTL8187SE_ANAPARAM_OFF);
	rtl_iowrite16(priv, REG_ANAPARAM3, state ? RTL8187SE_ANAPARAM3_ON : RTL8187SE_ANAPARAM3_OFF);

	rtl_iowrite8(priv, REG_CONFIG3, config3);

	rtl_set_eeprom_mode(priv, RTL_EEPROM_MODE_NORMAL);
}
