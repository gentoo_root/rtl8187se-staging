#include "rtl818x.h"
#include <linux/delay.h>
#include <linux/etherdevice.h>

u8 rtl_ioread8(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread8((u8 *)priv->map + offset);
}

u16 rtl_ioread16(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread16((u8 *)priv->map + offset);
}

u32 rtl_ioread32(struct rtl_priv *priv, uintptr_t offset)
{
	return ioread32((u8 *)priv->map + offset);
}

void rtl_iowrite8(struct rtl_priv *priv, uintptr_t offset, u8 data)
{
	iowrite8(data, (u8 *)priv->map + offset);
	wmb();
}

void rtl_iowrite16(struct rtl_priv *priv, uintptr_t offset, u16 data)
{
	iowrite16(data, (u8 *)priv->map + offset);
	wmb();
}

void rtl_iowrite32(struct rtl_priv *priv, uintptr_t offset, u32 data)
{
	iowrite32(data, (u8 *)priv->map + offset);
	wmb();
}

#define REG_EEPROM_CMD		0x50
enum rtl8187se_bits_eeprom_cmd {
	BIT_EEPROM_CMD_READ		= (1 << 0),
	BIT_EEPROM_CMD_WRITE		= (1 << 1),
	BIT_EEPROM_CMD_CK		= (1 << 2),
	BIT_EEPROM_CMD_CS		= (1 << 3),
	BIT_EEPROM_CMD_NORMAL		= (0 << 6),
	BIT_EEPROM_CMD_LOAD		= (1 << 6),
	BIT_EEPROM_CMD_PROGRAM		= (2 << 6),
	BIT_EEPROM_CMD_CONFIG		= (3 << 6),
};

static void rtl_eeprom_register_read(struct eeprom_93cx6 *eeprom)
{
	struct rtl_priv *priv = eeprom->data;
	u8 reg = rtl_ioread8(priv, REG_EEPROM_CMD);

	eeprom->reg_data_in = reg & BIT_EEPROM_CMD_WRITE;
	eeprom->reg_data_out = reg & BIT_EEPROM_CMD_READ;
	eeprom->reg_data_clock = reg & BIT_EEPROM_CMD_CK;
	eeprom->reg_chip_select = reg & BIT_EEPROM_CMD_CS;
}

static void rtl_eeprom_register_write(struct eeprom_93cx6 *eeprom)
{
	struct rtl_priv *priv = eeprom->data;
	u8 reg = BIT_EEPROM_CMD_PROGRAM;

	if (eeprom->reg_data_in)
		reg |= BIT_EEPROM_CMD_WRITE;
	if (eeprom->reg_data_out)
		reg |= BIT_EEPROM_CMD_READ;
	if (eeprom->reg_data_clock)
		reg |= BIT_EEPROM_CMD_CK;
	if (eeprom->reg_chip_select)
		reg |= BIT_EEPROM_CMD_CS;

	rtl_iowrite8(priv, REG_EEPROM_CMD, reg);
	rtl_ioread8(priv, REG_EEPROM_CMD);
	udelay(10);
}

struct eeprom_93cx6 *rtl_get_eeprom(struct rtl_priv *priv)
{
	struct eeprom_93cx6 *eeprom = kmalloc(sizeof(*eeprom), GFP_KERNEL);

	eeprom->data = priv;
	eeprom->register_read = rtl_eeprom_register_read;
	eeprom->register_write = rtl_eeprom_register_write;
	eeprom->width = PCI_EEPROM_WIDTH_93C46;

	return eeprom;
}

void rtl_set_eeprom_mode(struct rtl_priv *priv, enum rtl_eeprom_mode mode)
{
	rtl_iowrite8(priv, REG_EEPROM_CMD, mode << 6);
}

enum rtl_eeprom_mode rtl_get_eeprom_mode(struct rtl_priv *priv)
{
	return rtl_ioread8(priv, REG_EEPROM_CMD) >> 6;
}

void rtl_prepare_beacon(struct rtl_priv *priv)
{
	struct ieee80211_hw *dev = priv->dev;
	struct ieee80211_vif *vif = priv->vif;
	struct rtl_vif *vif_priv = (struct rtl_vif *)&vif->drv_priv;

	if (vif_priv->enable_beacon) {
		struct sk_buff *skb = ieee80211_beacon_get(dev, vif);
		if (skb) {
			struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
			u16 rate = ieee80211_get_tx_rate(dev, info)->bitrate;
			priv->ops->tx(priv, skb, 6, false, rate);
		}
	}
}

static u16 rtl_rates[] = { 10, 20, 55, 110, 60, 90, 120, 180, 240, 360, 480, 540, 720 };

u16 rtl_rate_to_hw_value(u16 rate)
{
	size_t i;
	for (i = 0; i < ARRAY_SIZE(rtl_rates); i++) {
		if (rtl_rates[i] == rate)
			return i;
	}
	return 3;
}

u16 rtl_hw_value_to_rate(u16 hw_value)
{
	return rtl_rates[clamp_t(size_t, hw_value, 0, ARRAY_SIZE(rtl_rates)-1)];
}

bool rtl_is_b_mode(u16 rate)
{
	return (rate <= 110 && rate != 60 && rate != 90) || rate == 220;
}

static u16 rtl_compute_tx_time(u16 len, u16 rate)
{
	u16 frame_time;

	if (!rate) {
		PRINT_ERR("rate == 0 in rtl_compute_tx_time()");
		return 0;
	}

	if (rtl_is_b_mode(rate)) {
		frame_time = (u16)(144+48+(len*8/(rate/10)));
		if (len*8 % (rate/10)) /* get the ceilling */
			frame_time++;
	} else {	/* 802.11g DSSS-OFDM PLCP length field calculation. */
		u16 dbps = rate * 2 / 5;
		u16 ceiling = (22 + 8*len) / dbps + (((22 + 8*len) % dbps) ? 1 : 0);
		frame_time = 26 + 4*ceiling;
	}
	return frame_time;
}

void rtl_get_tx_param(struct rtl_tx_param *tx_param, struct rtl_priv *priv, struct sk_buff *skb, int rate)
{
	struct ieee80211_hdr_3addr *frag_hdr = (struct ieee80211_hdr_3addr *)skb->data;
	size_t len = skb->len;
	u16 ack_time = rtl_compute_tx_time(14, 10);
	const u16 sCrcLng = 4;
	const u16 sAckCtsLng = 112;	/* bits in ACK and CTS frames */
	const u16 aSifsTime = 10;

	tx_param->rts_enable = false;
	tx_param->rts_duration = 0;

	if (is_multicast_ether_addr(frag_hdr->addr1)) {
		tx_param->tx_duration = rtl_compute_tx_time(len + sCrcLng, rate);
		return;
	}

	if (priv->rts && len+sCrcLng > priv->rts) {
		u16 rts_time = rtl_compute_tx_time(sAckCtsLng/8, rate);
		u16 cts_time = rtl_compute_tx_time(14, 10);
		tx_param->rts_enable = true;
		tx_param->rts_duration = rtl_compute_tx_time(len + sCrcLng, rate);
		tx_param->rts_duration += cts_time + ack_time + 3*aSifsTime;
		tx_param->tx_duration = rts_time + tx_param->rts_duration;
	} else {
		tx_param->tx_duration = rtl_compute_tx_time(len + sCrcLng, rate);
		tx_param->tx_duration += aSifsTime + ack_time;
	}

	if (frag_hdr->frame_control & IEEE80211_FCTL_MOREFRAGS) {
		frag_hdr->duration_id = rtl_compute_tx_time(len + sCrcLng, rate);
		frag_hdr->duration_id += 3*aSifsTime + 2*ack_time;
	} else
		frag_hdr->duration_id = aSifsTime + ack_time;
}
