#pragma once

#include "dev.h"
#include <linux/eeprom_93cx6.h>

/* Device registers access */
u8 rtl_ioread8(struct rtl_priv *priv, uintptr_t offset);
u16 rtl_ioread16(struct rtl_priv *priv, uintptr_t offset);
u32 rtl_ioread32(struct rtl_priv *priv, uintptr_t offset);
void rtl_iowrite8(struct rtl_priv *priv, uintptr_t offset, u8 data);
void rtl_iowrite16(struct rtl_priv *priv, uintptr_t offset, u16 data);
void rtl_iowrite32(struct rtl_priv *priv, uintptr_t offset, u32 data);

/* EEPROM access */
struct eeprom_93cx6 *rtl_get_eeprom(struct rtl_priv *priv);
enum rtl_eeprom_mode {
	RTL_EEPROM_MODE_NORMAL	= 0,
	RTL_EEPROM_MODE_LOAD	= 1,
	RTL_EEPROM_MODE_PROGRAM	= 2,
	RTL_EEPROM_MODE_CONFIG	= 3,
};
void rtl_set_eeprom_mode(struct rtl_priv *priv, enum rtl_eeprom_mode mode);
enum rtl_eeprom_mode rtl_get_eeprom_mode(struct rtl_priv *priv);

/* Beaconing */
void rtl_prepare_beacon(struct rtl_priv *priv);

/* Bitrate and hw_value conversion */
u16 rtl_rate_to_hw_value(u16 rate);
u16 rtl_hw_value_to_rate(u16 hw_value);
bool rtl_is_b_mode(u16 rate);

/* TX parameters */
struct rtl_tx_param {
	bool rts_enable;
	u16 rts_duration;
	u16 tx_duration;
};
void rtl_get_tx_param(struct rtl_tx_param *tx_param, struct rtl_priv *priv, struct sk_buff *skb, int rate);
